﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public static class EnumExtension
    {
        /// <summary>
        /// 获取枚举常数的名称。
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static string GetName(this Enum e)
        {
            string ret = Enum.GetName(e.GetType(), e);
            return ret;
        }
        /// <summary>
        /// 将枚举转换为 int 类型。
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static int ToInt(this Enum e)
        {
            int ret = Convert.ToInt32(e);
            return ret;
        }
        /// <summary>
        /// 将枚举转换成字典
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <returns></returns>
        public static Dictionary<int, string> GetDic<TEnum>()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();
            Type t = typeof(TEnum);
            var arr = Enum.GetValues(t);
            foreach (var item in arr)
            {
                dic.Add((int)item, item.ToString());
            }
            return dic;
        }

        public static bool Exist<TEnum>(string dec)
        {
            var result = new SortedList<int, string>();
            Type t = typeof(TEnum);
            Array arrays = Enum.GetValues(t);
            for (int i = 0; i < arrays.LongLength; i++)
            {
                object test = arrays.GetValue(i);
                FieldInfo fieldInfo = test.GetType().GetField(test.ToString());
                object[] attribArray = fieldInfo.GetCustomAttributes(false);
                DescriptionAttribute attrib = (DescriptionAttribute)attribArray[0];
                if(dec==attrib.Description) return true;
            }
            return false;
        }
    }
}

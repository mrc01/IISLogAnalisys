﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：DotNetEx.Helpers 
* 项目描述 ： 
* 类 名 称 ：UserHelper 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：DotNetEx.Helpers
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/2/17 0:01:26 
* 更新时间 ：2019/2/17 0:01:26 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetEx
{
    public class UserHelper
    {
        public static string GenUserSecretkey()
        {
            string no = CreateNo();
            string secretkey = no.ToMD5().Substring(8, 16).ToLower();
            return secretkey;
        }

        static string CreateNo()
        {
            Random random = new Random();
            string strRandom = random.Next(1000, 10000).ToString(); //生成编号 
            string code = DateTime.Now.ToString("yyyyMMddHHmmss") + strRandom;
            return code;
        }
    }
}

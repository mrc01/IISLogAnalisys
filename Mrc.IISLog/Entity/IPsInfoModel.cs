﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.IISLog.Entity 
* 项目描述 ： 
* 类 名 称 ：IPsInfoModel 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.IISLog.Entity
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/1/10 22:07:07 
* 更新时间 ：2019/1/10 22:07:07 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.IISLog
{
    public class IPsInfoModel
    {
        public string ip { get; set; }
        public int count { get; set; }
        public List<string> Paths { get; set;}

        public bool isWarning { get; set; } = false;
    }
}

﻿
using Mrc.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mrc.IISLog
{

    public class IISKeyWords:BaseEntity
    {
        public string keyword { get; set; }
        public string type { get; set; }
    }
}

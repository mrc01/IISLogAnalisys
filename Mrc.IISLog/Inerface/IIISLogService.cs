﻿using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.IISLog
{
    public interface IIISKeyWordsService : IServiceBase<IISKeyWords>, IMrcService
    {

    }
    public class IISKeyWordsService : ServiceBase<IISKeyWords>, IIISKeyWordsService
    {
        public IISKeyWordsService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}


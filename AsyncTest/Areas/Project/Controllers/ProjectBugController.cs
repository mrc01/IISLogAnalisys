﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetEx;
using DotNetEx.ExceptionEx;
using Microsoft.AspNetCore.Mvc;
using Mrc.Application;
using Mrc.Entity;
using Mrc.Entity.Models;
using MrcWeb.Controllers;
using Mrc.Data;

namespace Mrc.Web.Areas.Project.Controllers
{
    [Area("Project")]
    [Permission("user.projectbug")]
    public class ProjectBugController : BaseController
    {
        public readonly IUserProjectBugService _userProjectBugService;
        public ProjectBugController(IUserProjectBugService userProjectBugService)
        {
            _userProjectBugService = userProjectBugService;
        }

        #region 页面
        public IActionResult Index()
        {
            return View();
        }
        [Permission("user.projectbug.add")]
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }
        [Permission("user.projectbug.update")]
        [HttpGet]
        public ActionResult Update(string id)
        {
            return View(_userProjectBugService.Single(id));
        }
        #endregion

        #region 接口

        [Permission("user.projectbug.add")]
        [HttpPost]
        public JsonResult Add(AddProjectBugDto dto)
        {
            _userProjectBugService.AddBug(dto,CurrentSession);
            return Json(AjaxResult.CreateResult("添加成功"));
        }
        [Permission("user.projectbug.update")]
        [HttpPost]
        public JsonResult Update(UpdateProjectBugDto dto)
        {
            _userProjectBugService.UpdateBug(dto, CurrentSession);
            return Json(AjaxResult.CreateResult("更新成功"));
        }
        [Permission("user.projectbug.delete")]
        [HttpPost]
        public JsonResult Delete(string[] ids)
        {
            _userProjectBugService.DeleteBugs(ids,CurrentSession);
            return Json(AjaxResult.CreateResult("删除成功"));
        }
        [Permission("user.projectbug.updatestatus")]
        [HttpPost]
        public JsonResult UpdateStatus(string id,int status)
        {
            _userProjectBugService.UpdateStatus(id,status);
            return Json(AjaxResult.CreateResult("更新状态成功"));
        }
        #endregion

        #region 数据
        public ActionResult Models(int pageNumber, int pageSize)
        {
            LayuiTableJson<ProjectBugModels> layuiTableJson = new LayuiTableJson<ProjectBugModels>();
            var dbContext = DbContextHelper.GetDBContext();
            var query = dbContext.Query<UserProjectBug>().AdminFilter(CurrentSession, x => x.ExecuteUId == CurrentSession.UserId);
            layuiTableJson.count = query.Count();
            var data = query.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            data.ForEach((item) =>
            {
                layuiTableJson.data.Add(new ProjectBugModels
                {
                    Id = item.Id,
                    DemandName = item.DemandName,
                   // Priority = typeof(ProjectPriorityEnums).GetEnumName(item.Priority),
                    ProjectName = item.ProjectName,
                    Status = typeof(ProjectBugStatusEnum).GetEnumName(item.Status),
                    Name = item.Name,
                    ExecuteUser = item.ExecuteUser,
                    Remark = item.Remark,
                    SeverityLevel = typeof(ProjectBugSeverityLevelEnums).GetEnumName(item.SeverityLevel),
                    Type = typeof(ProjectBugTypeEnum).GetEnumName(item.Type),
                    TaskName=item.TaskName
                });
            });
            layuiTableJson.code = 0;
            layuiTableJson.msg = "";
            return Json(layuiTableJson);
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MrcWeb.Controllers;
using Microsoft.AspNetCore.Mvc;
using Mrc.Application;
using Mrc.Entity;
using DotNetEx;
using DotNetEx.ExceptionEx;
using Mrc.Entity.Models;

namespace MrcWeb.Areas.Project.Controllers
{
    [Area("Project")]
    [Login]
    public class ProjectTypeController : BaseController
    {
        private readonly IUserProjectTypeService _userProjectTypeService;
        public ProjectTypeController(IUserProjectTypeService userProjectTypeService)
        {
            this._userProjectTypeService = userProjectTypeService;
        }

        #region View
        public IActionResult Index()
        {
            return View();
        }
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult Update(string id)
        {
            return View(_userProjectTypeService.Single(id));
        }
        #endregion


        #region  Data
        [HttpGet]
        public ActionResult Models(int pageNumber = 1, int pageSize = 20, string keyword = "")
        {
            LayuiTableJson<UserProjectType> layuiTableJson = new LayuiTableJson<UserProjectType>();
            var allTypes = _userProjectTypeService.List().ToList();
            var typeDic = allTypes.ToDictionary(x => x.Id, x => x.Name);
            var query = allTypes.Where(x => x.Name.Contains(keyword));
            layuiTableJson.count = query.Count();
            layuiTableJson.code = 0;
            layuiTableJson.msg = "";
            List<UserProjectType> data = new List<UserProjectType>();
            query.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList().ForEach((thistype) =>
            {
                var ids = new List<string>();
                (thistype.TypeIds ?? "").Split(",").ToList().ForEach((id) =>
                {
                    if (!id.IsNullOrEmpty()) ids.Add(typeDic[id]);
                });
                thistype.TypeIds = string.Join(",", ids);
                data.Add(thistype);
            });
            layuiTableJson.data = data;
            return Json(layuiTableJson);
        }
        public JsonResult GetData()
        {
            var data = _userProjectTypeService.List().ToList().Select(x => new SelectModel
            {
                name = x.Id,
                value = x.Name,
                selected = "",
                disabled = ""
            }).ToList();
            return Json(new { code = 0, msg = "获取成功", data });
        }
        #endregion


        #region api
        [Permission("user.projecttype.add")]
        [HttpPost]
        public ActionResult Add(string name = "", string typeIds = "")
        {
            if (name.IsNullOrEmpty()) throw new InvalidInputException("请输入项目类型名");
            UserProjectType userProjectType = new UserProjectType();
            userProjectType.Id = IdHelper.CreateStringSnowflakeId();
            userProjectType.Name = name;
            userProjectType.TypeIds = typeIds;
            _userProjectTypeService.Add(userProjectType);
            return Json(AjaxResult.CreateResult("添加成功"));
        }
        [Permission("user.projecttype.update")]
        [HttpPost]
        public ActionResult Update(string id = "", string name = "", string typeIds = "")
        {
            if (id.IsNullOrEmpty()) throw new InvalidInputException("更新失败，类型不存在");
            if (name.IsNullOrEmpty()) throw new InvalidInputException("请输入项目类型名");
            var data = _userProjectTypeService.Single(id);
            if (data == null) throw new InvalidInputException("数据不存在");
            data.Name = name; data.TypeIds = typeIds;
            _userProjectTypeService.Update(data);
            return Json(AjaxResult.CreateResult("更新成功"));
        }

        [Permission("user.projecttype.delete")]
        [HttpPost]
        public ActionResult Delete(string[] ids)
        {
            foreach (var item in ids)
            {
                _userProjectTypeService.SoftDelete(item, this.CurrentSession.UserId);
            }
            return Json(AjaxResult.CreateResult("删除成功"));
        }
        #endregion


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MrcWeb.Controllers;
using Microsoft.AspNetCore.Mvc;
using Mrc.Application;
using Mrc.Entity.Models;
using Mrc.Entity;
using Mrc.Data;

namespace MrcWeb.Areas.Project.Controllers
{
    [Area("Project")]
    [Permission("user.projecttask")]
    [Login]
    public class ProjectTaskController : BaseController
    {
        private readonly IUserProjectTaskService _userProjectTaskService;
        private readonly IUserProjectService _userProjectService;
        private readonly IUserProjectMemberService _userProjectMemberService;
        private readonly ISysUserService _sysUserService;
        private readonly IUserProjectDemandService _userProjectDemandService;
        public ProjectTaskController(IUserProjectMemberService userProjectMemberService, 
            IUserProjectService userProjectService,
            IUserProjectTaskService userProjectTaskService,
            ISysUserService sysUserService,
            IUserProjectDemandService userProjectDemandService
            )
        {
            _userProjectDemandService = userProjectDemandService;
            _sysUserService = sysUserService;
            _userProjectMemberService = userProjectMemberService;
            _userProjectService = userProjectService;
            _userProjectTaskService = userProjectTaskService;
        }
        public ActionResult Index()
        {
            return View();
        }
        [Permission("user.projecttask.add")]
        public ActionResult Add(string id)
        {
            UserProjectDemand userProjectDemand = new UserProjectDemand();
            var demand = _userProjectDemandService.Single(id);
            if (demand != null) userProjectDemand = demand;
            return View(userProjectDemand);
        }
        [Permission("user.projecttask.update")]
        public ActionResult Update(string id)
        {
            return View(_userProjectTaskService.Single(id));
        }
        [Permission("user.projecttask.add")]
        [HttpPost]
        public ActionResult Add(AddProjectTaskDto  addProjectTaskDto)
        {
            _userProjectTaskService.AddTask(addProjectTaskDto, CurrentSession);
            return Json(AjaxResult.CreateResult("添加成功"));
        }
        [Permission("user.projecttask.update")]
        [HttpPost]
        public ActionResult Update(UpdateProjectTaskDto updateProjectTaskDto)
        {
            _userProjectTaskService.UpdateTask(updateProjectTaskDto, CurrentSession);
            return Json(AjaxResult.CreateResult("更新成功"));
        }
        public JsonResult Models(int pageNumber, int pageSize)
        {
            LayuiTableJson<ProjectTaskListModel> layuiTableJson = new LayuiTableJson<ProjectTaskListModel>();
            var dbContext = DbContextHelper.GetDBContext();
            var query = dbContext.Query<UserProjectMember>().Where(x => x.UserID == CurrentSession.UserId && x.TaskId != null);
            layuiTableJson.count = query.Count();
            var data = query.Skip((pageNumber - 1) * pageSize).Take(pageSize).InnerJoin<UserProjectTask>((member, task) => member.TaskId == task.Id).Select((membermodel, taskmodel) => taskmodel).ToList();
            data.ForEach((item) =>
            {
                layuiTableJson.data.Add(new ProjectTaskListModel
                {
                    Id = item.Id,
                    PlanFinishTime = item.PlanFinishTime,
                    PlanStarTime = item.PlanStarTime,
                    Process = item.Process,
                    DemandName = item.DemandName,
                    Priority = typeof(ProjectPriorityEnums).GetEnumName(item.Priority),
                    ProjectName = item.ProjectName,
                    Status = typeof(ProjectTaskStatusEnum).GetEnumName(item.Status),
                    Type = typeof(ProjectTaskTypeEnum).GetEnumName(item.Type),
                    Description = item.Description,
                    Name = item.Name
                });
            });
            layuiTableJson.code = 0;
            layuiTableJson.msg = "";
            return Json(layuiTableJson);
        }
        /// <summary>
        /// 获取我的任务
        /// </summary>
        /// <returns></returns>
        public async Task<JsonResult> GetTaskNames()
        {
            var result=await Task.Run(() =>
            {
                var dbContext = DbContextHelper.GetDBContext();
                var query = dbContext.Query<UserProjectMember>().Where(x => x.UserID == CurrentSession.UserId && x.TaskId != null);
                var data = query.InnerJoin<UserProjectTask>((member, task) => member.TaskId == task.Id)
                .Select((membermodel, taskmodel) =>new SelectModel{ value=taskmodel.Id,name=taskmodel.Name, disabled="",selected=""}).ToList();
                return data;
            });
            return Json(new { code = 0, msg = "获取成功",data=result});
        }

        public JsonResult GetSelectData()
        {
            var data = _sysUserService.List(x => x.Id != string.Empty).ToList().Select(x => new SelectModel
            {
                name = x.Name,
                value = x.Id,
                selected = "",
                disabled = ""
            }).ToList();
            return Json(new { code = 0, msg = "获取成功", data });
        }
        public JsonResult TaskUsers(string id)
        {
            var data = _userProjectMemberService.List(x => x.TaskId == id).Select(x => x.UserID).ToList();
            return Json(AjaxResult.CreateResult<List<string>>(ResultStatus.OK, data));
        }
    }
}
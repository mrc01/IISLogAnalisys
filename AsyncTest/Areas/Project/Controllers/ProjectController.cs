﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MrcWeb.Controllers;
using DotNetEx;
using DotNetEx.ExceptionEx;
using Microsoft.AspNetCore.Mvc;
using Mrc.Application;
using Mrc.Entity;
using Mrc.Entity.Models;

namespace MrcWeb.Areas.Project.Controllers
{
    [Area("Project")]
    [Permission("user.project")]
    public class ProjectController : BaseController
    {
        private readonly IUserProjectService _userProjectService;
        private readonly IUserProjectMemberService _userProjectMemberService;
        public ProjectController(
            IUserProjectService userProjectService,
            IUserProjectMemberService userProjectMemberService
            )
        {
            _userProjectService = userProjectService;
            _userProjectMemberService = userProjectMemberService;
        }
        public IActionResult Index()
        {
            ViewBag.statusName = JsonHelper.Serialize(EnumExtension.GetDic<ProjectStatusEnum>());
            return View();
        }
        public JsonResult GetMyProject(int pageNumber, int pageSize)
        {
            var result = _userProjectService.List().ToLayuiTable(pageNumber, pageSize);
            return Json(result);
        }
        [Permission("user.project.add")]
        public IActionResult Add()
        {
            return View();
        }

        [Permission("user.project.update")]
        public IActionResult Update(string id)
        {
            return View(_userProjectService.Single(id));
        }

        [Permission("user.project.add")]
        [HttpPost]
        public ActionResult Add(UserProject input)
        {
            input.Id = IdHelper.CreateStringSnowflakeId();
            if (input.Name.IsNullOrEmpty()) throw new InvalidInputException("请输入项目类型名");
            if ((input.Amount ?? 0.0M) == 0.0M) throw new InvalidInputException("请输入项目金额");
            if (input.TypeId.IsNullOrEmpty()) throw new InvalidInputException("请输入选择项目类型");
            if ((input.Status ?? 0) < 0) throw new InvalidInputException("请选中当前项目状态");
            _userProjectService.Add(input);
            return Json(AjaxResult.CreateResult("修改成功"));
        }
        [Permission("user.project.update")]
        [HttpPost]
        public ActionResult Update(UserProject input)
        {
            var project = _userProjectService.Single(input.Id);
            if(project==null) throw new InvalidInputException("项目不存在");
            if (input.Name.IsNullOrEmpty()) throw new InvalidInputException("请输入项目类型名");
            if ((input.Amount ?? 0.0M) == 0.0M) throw new InvalidInputException("请输入项目金额");
            if (input.TypeId.IsNullOrEmpty()) throw new InvalidInputException("请输入选择项目类型");
            if ((input.Status ?? 0)< 0) throw new InvalidInputException("请选中当前项目状态");
            project.Name = input.Name;
            project.Amount = input.Amount;
            project.TypeId = input.TypeId;
            project.Status = input.Status;
            project.Description = input.Description;
            project.TemplateProjectID = input.TemplateProjectID;
            _userProjectService.Update(project);
            return Json(AjaxResult.CreateResult("修改成功"));
        }
        [Permission("user.project.delete")]
        [HttpPost]
        public ActionResult Delete(string[] ids)
        {
            foreach (var item in ids)
            {
                _userProjectService.SoftDelete(item, this.CurrentSession.UserId);
            }
            return Json(AjaxResult.CreateResult(ResultStatus.OK, "删除成功"));
        }
        public JsonResult GetSelectData(string keyword = "",string id="")
        {
            keyword = keyword.IsNullOrEmpty() ? "" : keyword;
            var data = _userProjectService.List(x=>x.Id!=string.Empty).ToList().Select(x => new SelectModel
            {
                name = x.Name,
                value = x.Id,
                selected =x.Id==keyword? "selected" : "",
                disabled = ""
            }).ToList();
            return Json(new { code = 0, msg = "获取成功",data});
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetEx;
using DotNetEx.ExceptionEx;
using Microsoft.AspNetCore.Mvc;
using Mrc.Application;
using Mrc.Entity;
using Mrc.Entity.Models;
using MrcWeb.Controllers;
using Mrc.Data;
namespace MrcWeb.Areas.Project.Controllers
{
    [Area("Project")]
    [Login]
    public class ProjectDemandController : BaseController
    {
        private readonly IUserProjectTaskService _userProjectTaskService;
        private readonly IUserProjectService _userProjectService;
        private readonly IUserProjectMemberService _userProjectMemberService;
        private readonly ISysUserService _sysUserService;
        private readonly IUserProjectDemandService _userProjectDemandService;
        public ProjectDemandController(
            IUserProjectMemberService userProjectMemberService,
            IUserProjectService userProjectService,
            IUserProjectTaskService userProjectTaskService,
            ISysUserService sysUserService,
            IUserProjectDemandService userProjectDemandService
            )
        {
            _userProjectDemandService = userProjectDemandService;
            _sysUserService = sysUserService;
            _userProjectMemberService = userProjectMemberService;
            _userProjectService = userProjectService;
            _userProjectTaskService = userProjectTaskService;
        }

        #region 页面
        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 查看需求
        /// </summary>
        /// <returns></returns>
        public IActionResult LookDemand()
        {
            return View();
        }
        /// <summary>
        /// 添加需求
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Permission("user.projectdemand.add")]
        public IActionResult Add(string id)
        {
            return View(_userProjectService.Single(id));
        }
        /// <summary>
        /// 更新需求
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Permission("user.projectdemand.update")]
        public IActionResult Update(string id)
        {
            return View(_userProjectDemandService.Single(id));
        }

        #endregion


        #region 接口


        [Permission("user.projectdemand.add")]
        [HttpPost]
        public ActionResult Add(AddUpdateUserProjectDemand input)
        {
            _userProjectDemandService.AddDemand(input, this.CurrentSession.UserId);
            return Json(AjaxResult.CreateResult("修改成功"));
        }
        [Permission("user.projectdemand.update")]
        [HttpPost]
        public ActionResult Update(AddUpdateUserProjectDemand input)
        {
            _userProjectDemandService.UpdateDemand(input, CurrentSession.UserId);
            return Json(AjaxResult.CreateResult("修改成功"));
        }
        [Permission("user.projectdemand.delete")]
        [HttpPost]
        public ActionResult Delete(string[] ids)
        {
            foreach (var item in ids)
            {
                _userProjectDemandService.SoftDelete(item, this.CurrentSession.UserId);
            }
            return Json(AjaxResult.CreateResult(ResultStatus.OK, "删除成功"));
        }


        #endregion

        #region 数据

        public JsonResult GetSelectData()
        {
            var data = _sysUserService.List(x => x.Id != string.Empty).ToList().Select(x => new SelectModel
            {
                name = x.Name,
                value = x.Id,
                selected = "",
                disabled = ""
            }).ToList();
            return Json(new { code = 0, msg = "获取成功", data });
        }
        public ActionResult Models(int pageNumber, int pageSize)
        {
            LayuiTableJson<UserProjectDemandDto> layuiTableJson = new LayuiTableJson<UserProjectDemandDto>();

            var dbContext = DbContextHelper.GetDBContext();
            var query = dbContext.Query<UserProjectDemand>().AdminFilter(CurrentSession, x => x.ExecuteUId == CurrentSession.UserId);
            layuiTableJson.count = query.Count();
            layuiTableJson.code = 0;
            layuiTableJson.msg = "";
            var data = query.Skip((pageNumber - 1) * pageSize).Take(pageSize).InnerJoin<SysUser>((demand, user) => demand.ExecuteUId == user.Id).Select((demandmodel, usermodel) => new { demandmodel, usermodel }).ToList();
            data.ForEach((item) =>
            {
                layuiTableJson.data.Add(new UserProjectDemandDto
                {
                    Endtime = item.demandmodel.Endtime,
                    ExecuteUser = item.usermodel.Name,
                    Id = item.demandmodel.Id,
                    Priority = typeof(ProjectPriorityEnums).GetEnumName(item.demandmodel.Priority),
                    ProjectName = item.demandmodel.ProjectName,
                    Status = typeof(ProjectStatusEnum).GetEnumName(item.demandmodel.Status),
                    Type = typeof(ProjectDemandTypeEnums).GetEnumName(item.demandmodel.Type),
                    Description = item.demandmodel.Description,
                    Name = item.demandmodel.Name
                });
            });
            return Json(layuiTableJson);
        }
        public ActionResult DemandUsers(string id)
        {
            var data = _userProjectMemberService.List(x => x.ProjectDemandId == id).Select(x => x.UserID).ToList();
            return Json(AjaxResult.CreateResult<List<string>>(ResultStatus.OK, data));
        }
        #endregion


    }
}
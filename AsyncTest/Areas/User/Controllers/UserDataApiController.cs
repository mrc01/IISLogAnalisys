﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mrc.Data;
using Mrc.Entity;
using Mrc.Entity.Models;
using MrcWeb.Controllers;

namespace Mrc.Web.Areas.User.Controllers
{
    [Area("User")]
    public class UserDataApiController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<JsonResult> GetUserNames()
        {
            var result = await Task.Run(() =>
            {
                var dbContext = DbContextHelper.GetDBContext();
                var data = dbContext.Query<SysUser>().Where(x => x.IsDeleted != true).Select(x => new { x.Id, x.Name }).ToList().Select(x => new SelectModel
                {
                    name = x.Name,
                    value = x.Id,
                    selected = "",
                    disabled = ""
                }).ToList();
                return data;
            });
            return Json(new { code = 0, msg = "获取成功", data = result });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MrcWeb.Controllers;
using DotNetEx;
using DotNetEx.ExceptionEx;
using Microsoft.AspNetCore.Mvc;
using Mrc.Application;
using Mrc.Entity;

namespace MrcWeb.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Permission("system.post")]
    public class PostController : BaseController
    {
        private readonly ISysOrgService _sysOrgService;
        private readonly ISysPostService _sysPostService;
        public PostController(ISysOrgService sysOrgService,ISysPostService sysPostService)
        {
            this._sysOrgService = sysOrgService;
            this._sysPostService = sysPostService;
        }
        public ActionResult Index()
        {
            var allOrgName = _sysOrgService.List(x =>(x.IsDeleted!=true||x.IsDeleted==null)).ToList().ToDictionary(x=>x.Id,x=>x.Name);
            ViewBag.orgName = JsonHelper.Serialize(allOrgName);
            return View();
        }

        [HttpGet]
        public ActionResult Models(string keyword)
        {
                var result = _sysPostService.List(x => x.IsDeleted != true).ToLayuiTable(1, 1000);
                return Json(result);
        }

        [Permission("system.post.add")]
        public ActionResult add()
        {
            List<SysOrg> orgs = _sysOrgService.List(x => x.IsDeleted != true).ToList();
            ViewBag.orgs = orgs;
            return View();
        }

        [Permission("system.post.update")]
        public ActionResult update(string id)
        {
            var post = _sysPostService.Single(x => x.Id == id);
            ViewBag.data = post.Serialize();
            List<SysOrg> orgs = _sysOrgService.List(x => x.IsDeleted != true).ToList();
            ViewBag.orgs = orgs;
            return View();
        }

        [Permission("system.post.add")]
        [HttpPost]
        public ActionResult Add(SysPost input)
        {
            try
            {
                input.Id = IdHelper.CreateStringSnowflakeId();
                input.CreateTime = DateTime.Now;
                if (input.Name.IsNullOrEmpty()) throw new MrcException("岗位名称不能为空");
                if (input.OrgId.IsNullOrEmpty()) throw new MrcException("请选择岗位");
                _sysPostService.Add(input);
                return Json(AjaxResult.CreateResult("添加成功"));
            }
            catch (MrcException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed,ex.Message));
            }
        }

        [Permission("system.post.update")]
        [HttpPost]
        public ActionResult Update(SysPost input)
        {
            try
            {
                if (input.Id.IsNullOrEmpty()) throw new MrcException("岗位不存在");
                var post = _sysPostService.Single(x => x.Id == input.Id);
                if (post==null) throw new MrcException("岗位不存在");
                if (input.Name.IsNullOrEmpty()) throw new MrcException("岗位名称不能为空");
                if (input.OrgId.IsNullOrEmpty()) throw new MrcException("请选择岗位");
                post.Name = input.Name;
                post.OrgId = input.OrgId;
                post.Description = input.Description;
                _sysPostService.Update(post);
                return Json(AjaxResult.CreateResult("修改成功"));
            }
            catch (MrcException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed, ex.Message));
            }
        }

        [Permission("system.post.delete")]
        [HttpPost]
        public ActionResult Delete(string[] ids)
        {
            try
            {
                foreach (var item in ids)
                {
                    if (item.IsNullOrEmpty()) throw new MrcException("岗位不存在");
                    _sysPostService.SoftDelete(item, this.CurrentSession.UserId);
                }
                return Json(AjaxResult.CreateResult("删除成功"));
            }
            catch (MrcException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed, ex.Message));
            }
        }
    }
}
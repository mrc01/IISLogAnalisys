﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MrcWeb.MultipartRequest;
using Mrc.Entity;
using Mrc.Data.DBContext;
using Mrc.Data;
using Mrc.Application;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using MrcWeb.Controllers;
using DotNetEx.Helpers;
using DotNetEx;
using Microsoft.Extensions.Caching.Memory;
using Mrc.Application.Helper;

namespace MrcWeb.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Login]
    public class HomeController : BaseController
    {
        private readonly ISysOrgService _sysOrgService;
        private readonly ISysPermissionService _sysPermissionService;
        private readonly ISysUserService _sysUserService;
        public HomeController(ISysOrgService sysOrgService,ISysPermissionService sysPermissionService,ISysUserService sysUserService)
        {
            this._sysUserService = sysUserService;
            this._sysOrgService = sysOrgService;
            this._sysPermissionService = sysPermissionService;
        }

        public IActionResult Index()
        {
            return View(this.CurrentSession);
        }

        public async Task<JsonResult> LeftNav()
        {
            string key = $"permissions_nav:{this.CurrentSession.UserId}";
            List<SysPermission> sysPermissionList = _sysPermissionService.GetList().Where(x=>x.Type==(int)PermissionType.公共菜单||x.Type==(int)PermissionType.节点组||x.Type==(int)PermissionType.权限菜单).ToList();
            List<string> myPermission = new List<string>();
            if(!CacheHelper.memoryCache.TryGetValue(key,out myPermission))
            {
                myPermission = _sysUserService.GetUserPermissions(this.CurrentSession.UserId).Where(x => x.Type == (int)PermissionType.公共菜单 || x.Type == (int)PermissionType.节点组 || x.Type == (int)PermissionType.权限菜单).Select(x => x.Id).ToList();
                CacheHelper.memoryCache.Set<List<string>>(key, myPermission,TimeSpan.FromMinutes(5));
            }
            if (this.CurrentSession.IsAdmin&&myPermission.Count==0)
            {
                myPermission = sysPermissionList.Select(x => x.Id).ToList();
                CacheHelper.memoryCache.Set<List<string>>(key, myPermission, TimeSpan.FromMinutes(30));
            }
            NavDtreeHelper dtreeHelper = new NavDtreeHelper(this.CurrentSession,sysPermissionList,myPermission.ToDictionary(x=>x,x=>x));
            var datas =await dtreeHelper.GetData();
            dtreeHelper = null;
            return Json(new { status = new { code = ResultStatus.OK, message = "ok" }, data = datas });
        }
        public IActionResult Welcome()
        {
            return View();
        }

        [HttpPost]
        [DisableFormValueModelBinding]
        public async Task<IActionResult> upload()
        {
            FormValueProvider formModel;
            formModel = await Request.StreamFiles(@"D:\UploadingFiles");
            return Json(AjaxResult.CreateResult(ResultStatus.OK,"上传完成"));
        }

        public IActionResult testUpload()
        {
            return View();
        }

        public JsonResult testDBBack()
        {
            DBBackHelper.DBBack();
            //DBBackHelper.RestoreDatabase("");
            return Json("s");
        }

        public ActionResult LoginOut()
        {
            this.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect("/Login");
        }

    }
    public class MyViewModel
    {
        public string Username { get; set; }
    }
}
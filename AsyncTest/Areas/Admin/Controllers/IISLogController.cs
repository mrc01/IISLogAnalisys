﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mrc.IISLog;

namespace MrcWeb.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class IISLogController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult LogList()
        {
            return View();
        }
        public ActionResult IISVisit()
        {
            return View();
        }
        public async Task<JsonResult> GetLogIps(string name)
        {
            return Json(await IISManager.GetLogIps(name));
        }

        public async Task<JsonResult> GetIISCount(string name = "")
        {
            return Json(await IISManager.GetIISVisitCountMinutes(name));
        }
        public async Task<JsonResult> ReloadData()
        {
            return Json(await IISManager.ReloadData());
        }
        
    }
}
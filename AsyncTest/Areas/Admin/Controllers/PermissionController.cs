﻿using DotNetEx.ExceptionEx;
using DotNetEx.Helpers;
using Microsoft.AspNetCore.Mvc;
using Mrc.Application;
using Mrc.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using DotNetEx;
namespace MrcWeb.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Permission("system.permission")]
    [Login]
    public class PermissionController : Controller
    {

        private readonly ISysPermissionService _sysPermissionService;
        public PermissionController(ISysPermissionService sysPermissionService)
        {
            this._sysPermissionService = sysPermissionService;
        }

        public IActionResult Index()
        {
            List<PermissionModel> menuGroups = this.GetPermissionModels();
            this.ViewBag.MenuGroups = menuGroups;
            return View();
        }

        public ActionResult Models(string keyword)
        {
            var data = _sysPermissionService.GetList();
            DtreeHelper<SysPermission> dtreeHelper = new DtreeHelper<SysPermission>(data,EnumExtension.GetDic<PermissionType>());
            var datas = dtreeHelper.GetData();
            dtreeHelper = null;
            return Json(new{status=new {code=ResultStatus.OK, message="ok"},data=datas});
        }

        public ActionResult MenuGroups()
        {
            List<PermissionModel> models = this.GetPermissionModels();
            return Json(AjaxResult.CreateResult<List<PermissionModel>>(ResultStatus.OK,"",models));
        }

        List<PermissionModel> GetPermissionModels()
        {
            List<SysPermission> data = _sysPermissionService.GetList(PermissionType.节点组);

            foreach (var item in data)
            {
                item.Children.AddRange(data.Where(a => a.ParentId == item.Id));
            }

            var rootGroups = data.Where(a => a.ParentId == null).ToList();

            List<PermissionModel> models = new List<PermissionModel>();
            this.Fill(models, rootGroups, 0);

            return models;
        }
        void Fill(List<PermissionModel> container, List<SysPermission> permissions, int level)
        {
            foreach (var permission in permissions)
            {
                container.Add(PermissionModel.Create(permission, level));
                Fill(container, permission.Children, level + 1);
            }
        }

        [Permission("system.permission.add")]
        public IActionResult Add()
        {
            return View();
        }

        [Permission("system.permission.update")]
        public IActionResult Update()
        {
            return View();
        }

        [Permission("system.permission.add")]
        [HttpPost]
        public ActionResult Add(SysPermission input)
        {
            try
            {
                input.Id = IdHelper.CreateStringSnowflakeId();
                string msg = string.Empty;
                _sysPermissionService.isHadPermissionCode(input.Code, input.Id, out msg);
                if (!msg.IsNullOrEmpty())
                    throw new MrcException(msg);
                _sysPermissionService.Add(input);
                return Json(AjaxResult.CreateResult("修改成功"));
            }
            catch (MrcException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed, ex.Message));
            }
            catch (ArgumentException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed, ex.Message));
            }
        }
        [Permission("system.permission.update")]
        [HttpPost]
        public ActionResult Update(SysPermission input)
        {
            try
            {
                if (input.Id.IsNullOrEmpty())
                    throw new MrcException("无法获取该权限");
                string msg = string.Empty;
                _sysPermissionService.isHadPermissionCode(input.Code, input.Id, out msg);
                if (!msg.IsNullOrEmpty())
                    throw new MrcException(msg);
                _sysPermissionService.Update(input);
                return Json(AjaxResult.CreateResult("修改成功"));
            }
            catch (MrcException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed, ex.Message));
            }
            catch (ArgumentException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed, ex.Message));
            }
        }

        [Permission("system.permission.delete")]
        [HttpPost]
        public ActionResult Delete(string id)
        {
            try
            {
                if (id.IsNullOrEmpty())
                    throw new MrcException("无法获取权限码");
                if (_sysPermissionService.List(x => x.ParentId == id).Any())
                    throw new MrcException("删除失败！操作的对象包含了下级数据");
                _sysPermissionService.Delete(x=>x.Id==id);
                return Json(AjaxResult.CreateResult("删除成功"));
            }
            catch (MrcException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed,ex.Message));
            }
        }
    }
}
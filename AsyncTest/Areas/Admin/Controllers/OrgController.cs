﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MrcWeb.Controllers;
using DotNetEx.ExceptionEx;
using DotNetEx.Helpers;
using Microsoft.AspNetCore.Mvc;
using Mrc.Application;
using Mrc.Entity;
using Mrc.Entity.Enums;
using DotNetEx;
namespace MrcWeb.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Login]
    public class OrgController : BaseController
    {

        private readonly ISysOrgService _sysOrgService;
        private readonly ISysOrgTypeService _sysOrgTypeService;
        private readonly ISysPermissionService _sysPermissionService;
        public OrgController(ISysOrgService sysOrgService, ISysOrgTypeService sysOrgTypeService,ISysPermissionService sysPermissionService)
        {
            this._sysOrgService = sysOrgService;
            this._sysOrgTypeService = sysOrgTypeService;
            this._sysPermissionService = sysPermissionService;
        }
        public ActionResult Index()
        {
            List<SysOrgType> orgTypes = _sysOrgTypeService.List(x=>x.Id!=string.Empty).ToList();
            this.ViewBag.OrgTypes = orgTypes;
            return View();
        }


        public ActionResult Models(string keyword)
        {
            var data = _sysOrgService.List(x => x.Id != string.Empty).ToList();
            DtreeHelper<SysOrg> orgDtreeHelper = new DtreeHelper<SysOrg>(data,EnumExtension.GetDic<OrgType>());
            return Json(new { status = new { code = ResultStatus.OK, message = "ok" }, data = orgDtreeHelper.GetData()});
        }

        [Permission("system.org.add")]
        [HttpPost]
        public ActionResult Add(SysOrg input)
        {
            try
            {
                input.Id = IdHelper.CreateStringSnowflakeId();
                input.CreateTime = DateTime.Now;
                if ((input.Type ?? 0) == 0) throw new MrcException("请选择所属组织");
                if (input.Name.IsNullOrEmpty()) throw new MrcException("请输入组织名称");
                _sysOrgService.Add(input);
                return Json(AjaxResult.CreateResult(ResultStatus.OK, "添加成功"));
            }
            catch (MrcException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed,ex.Message));
            }
        }

        [Permission("system.org.update")]
        [HttpPost]
        public ActionResult Update(SysOrg input)
        {
            try
            {
                if ((input.Type ?? 0) == 0) throw new MrcException("请选择所属组织");
                if (input.Name.IsNullOrEmpty()) throw new MrcException("请输入组织名称");
                var org = _sysOrgService.Single(input.Id);
                if (org == null) throw new MrcException("组织不存在");
                org.Name = input.Name;
                org.Description = input.Description;
                org.ParentId = input.ParentId;
                org.Type = input.Type;
                _sysOrgService.Update(input);
                return Json(AjaxResult.CreateResult(ResultStatus.OK, "修改成功"));
            }
            catch (MrcException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed, ex.Message));
            }
        }

        [Permission("system.org.delete")]
        [HttpPost]
        public ActionResult Delete(string id)
        {
            if(id.IsNullOrEmpty()) return Json(AjaxResult.CreateResult(ResultStatus.Failed, "id不能为空"));
            _sysOrgService.Delete(id, this.CurrentSession.UserId);
            return Json(AjaxResult.CreateResult(ResultStatus.OK, "删除成功"));
        }

        public ActionResult GetPermissionTree(string id)
        {
            List<SysPermission> permissionList =_sysPermissionService.List(x=>x.IsDeleted!=true||x.IsDeleted==null).ToList();
            List<SysOrgPermission> orgPermissionList = new List<SysOrgPermission>();
            if (!string.IsNullOrEmpty(id))
            {
                orgPermissionList = _sysOrgService.GetPermissions(id);
            }
            List<string> permissions = orgPermissionList.Select(x => x.PermissionId).ToList();
            DtreeHelper<SysPermission> dtreeHelper = new DtreeHelper<SysPermission>(permissionList, EnumExtension.GetDic<PermissionType>(), permissions);
            var datas = dtreeHelper.GetData();
            dtreeHelper = null;
            return Json(new { status = new { code = ResultStatus.OK, message = "ok" }, data = datas });
        }

        [Permission("system.org.set_permission")]
        [HttpPost]
        public ActionResult SetPermission(string id, string[] permissions)
        {
            List<string> permissionList = permissions.ToList();
            _sysOrgService.SetPermission(id, permissionList);
            return Json(AjaxResult.CreateResult(ResultStatus.OK,"设置成功"));
        }

        public ActionResult GetParentOrgs(string orgType)
        {
            List<SysOrg> orgs = new List<SysOrg>();
            if (orgType == null)
                return  Json(AjaxResult.CreateResult<List<SysOrg>>(ResultStatus.OK,orgs));
             orgs = _sysOrgService.GetParentOrgs(orgType);
            return Json(AjaxResult.CreateResult<List<SysOrg>>(ResultStatus.OK, orgs));
        }
    }
}
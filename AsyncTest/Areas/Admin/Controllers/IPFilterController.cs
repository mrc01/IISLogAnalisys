﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MrcWeb.Controllers;
using DotNetEx;
using DotNetEx.ExceptionEx;
using Microsoft.AspNetCore.Mvc;
using Mrc.Application;
using Mrc.Entity;
namespace MrcWeb.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Login]
    public class IPFilterController : BaseController
    {
        private readonly ISysFilterIPService _sysFilterIPService;
        public IPFilterController(ISysFilterIPService sysFilterIPService)
        {
            this._sysFilterIPService = sysFilterIPService;
        }
        // GET: /<controller>/
        public ActionResult Index()
        {      
            return View();
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Update()
        {
            return View();
        }

        public  JsonResult GetIPFilterData(int pageNumber,int pageSize)
        {
            var filterIPList=_sysFilterIPService.List(x=>x.Id!=string.Empty).ToLayuiTable(pageNumber,pageSize);
            return Json(filterIPList);
        }
        [HttpPost]
        [Permission("system.ipfilter.add")]
        public JsonResult Add(SysFilterIP ipdata)
        {       
            try
            {
                if(_sysFilterIPService.List(x => x.StartIP == ipdata.StartIP).Any())
                throw new MrcException("IP已在过滤列表,无需重复添加");
                SysFilterIP sysFilterIP = new SysFilterIP();
                sysFilterIP.Id = IdHelper.CreateStringSnowflakeId();
                sysFilterIP.IsEnable = ipdata.IsEnable;
                sysFilterIP.StartIP = ipdata.StartIP;
                sysFilterIP.Description = ipdata.Description;
                sysFilterIP.CreateTime = DateTime.Now;
                sysFilterIP.CreateUserId = CurrentSession.UserId;
                sysFilterIP.IsDeleted = false;
                _sysFilterIPService.Add(sysFilterIP);

               // RedisHelper.HashSet("filter:ip", sysFilterIP.StartIP, "");

                return Json(AjaxResult.CreateResult(ResultStatus.OK, "添加成功"));
            }
            catch (MrcException ex)
            {           
                return Json(AjaxResult.CreateResult(ResultStatus.Failed, ex.Message));
            }                     
        }
        [HttpPost]
        [Permission("system.ipfilter.update")]
        public JsonResult Update(SysFilterIP ipdata)
        {
            var filterIP=_sysFilterIPService.Single(x => x.Id == ipdata.Id);
            filterIP.StartIP = ipdata.StartIP;
            filterIP.IsEnable = ipdata.IsEnable;
            filterIP.Description = ipdata.Description;
           // _sysFilterIPService.Update(filterIP);
            RedisHelper.HashSet("filter:ip",filterIP.StartIP,"");
            return Json(AjaxResult.CreateResult(ResultStatus.OK, "更新成功"));
        }
        [HttpPost]
        [Permission("system.ipfilter.delete")]
        public JsonResult Delete(string[] ids)
        {
            try
            {
                if (ids.Length == 0)
                    throw new MrcException("请选择需删除项");
                var filterIps = _sysFilterIPService.List(x => ids.Contains(x.Id)).ToList();
                foreach (var filterIp in filterIps)
                {
                    filterIp.IsDeleted = true;
                    filterIp.DeleteTime = DateTime.Now;
                    filterIp.DeleteUserId = CurrentSession.UserId;
                    _sysFilterIPService.Update(filterIp);
                  //  RedisHelper.HashDelete("filter:ip",filterIp.StartIP);
                }
                return Json(AjaxResult.CreateResult(ResultStatus.OK,"删除成功"));
            }
            catch (MrcException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed,ex.Message));
            }
        }

        [Permission("system.ipfilter.update")]
        public JsonResult GetData(string id)
        {
            return Json(AjaxResult.CreateResult<SysFilterIP>(ResultStatus.OK, "OK",_sysFilterIPService.Single(x=>x.Id==id)));
        }
    }
}

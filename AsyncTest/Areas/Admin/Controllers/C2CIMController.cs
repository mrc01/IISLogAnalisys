﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MrcWeb.MultipartRequest;
using Mrc.Entity;
using Mrc.Data.DBContext;
using Mrc.Data;
using Mrc.Application;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using MrcWeb.Controllers;
using DotNetEx.Helpers;
using DotNetEx;
using Microsoft.Extensions.Caching.Memory;
using DotNetEx.ExceptionEx;
using Mrc.RabbitMq;
using Mrc.RabbitMq.Helper;

namespace Mrc.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Login]
    public class C2CIMController : BaseController
    {

        private readonly ISysUserService _sysUserService;
        private readonly ISysC2CIMService _sysC2CIM;
        public C2CIMController(ISysUserService sysUserService,ISysC2CIMService sysC2CIM)
        {
            this._sysUserService = sysUserService;
            this._sysC2CIM = sysC2CIM;
        }
        public IActionResult Index(string tuid, string fuid)
        {
            ViewBag.fuid = fuid ?? "xxx";
            ViewBag.tuid = tuid ?? "_xxx";
            if (tuid.GetHashCode() > fuid.GetHashCode())
            {
                ViewBag.chatID = tuid + fuid;
            }
            else
            {
                ViewBag.chatID = fuid+ tuid;
            }
            return View();
        }
        [HttpPost]
        public JsonResult Send(string tuid, string fuid,string msg)
        {
            try
            {
                UserInfoModel tUserInfo = new UserInfoModel();
                UserInfoModel fUserInfo = new UserInfoModel();
                CacheHelper.memoryCache.TryGetValue(tuid, out tUserInfo);
                CacheHelper.memoryCache.TryGetValue(fuid, out fUserInfo);
                if(tUserInfo==null)
                {
                    var user = _sysUserService.Single(x => x.Id == tuid);
                    if (user != null)
                    {
                        tUserInfo = new UserInfoModel();
                        tUserInfo.Id = user.Id;
                        tUserInfo.Name = user.Name;
                        CacheHelper.memoryCache.Set<UserInfoModel>(tuid, tUserInfo);
                    }
                }
                if(fUserInfo == null)
                {
                    var user = _sysUserService.Single(x => x.Id == fuid);
                    if (user != null)
                    {
                        fUserInfo = new UserInfoModel();
                        fUserInfo.Id = user.Id;
                        fUserInfo.Name = user.Name;
                        CacheHelper.memoryCache.Set<UserInfoModel>(fuid, fUserInfo);
                    }
                }
                SysC2CIM sysC2CIM = new SysC2CIM();
                sysC2CIM.MsgType = 1;
                sysC2CIM.Msg = msg;
                sysC2CIM.FromUid = fUserInfo.Id;
                sysC2CIM.FromName = fUserInfo.Name;
                sysC2CIM.ToUid = tUserInfo.Id;
                sysC2CIM.ToName=tUserInfo.Name;
                sysC2CIM.Time = DateTime.Now.ToTimeStamp13();
                sysC2CIM.IsRead = false;
                _sysC2CIM.Add(sysC2CIM);
                string chatID = "";
                if (tuid.GetHashCode() > fuid.GetHashCode())
                {
                    chatID = tuid + fuid;
                }
                else
                {
                    chatID = fuid + tuid;
                }
                RabbitMQPushHelper.PushMessage(JsonHelper.Serialize(sysC2CIM),chatID, "amq.topic", ExchangeType.topic);
                return Json(AjaxResult.CreateResult(ResultStatus.OK, "添加成功"));
            }
            catch (MrcException ex)
            {
                return Json(AjaxResult.CreateResult(ResultStatus.Failed, ex.Message));
            }
        }



    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MrcWeb.Controllers;
using DotNetEx;
using DotNetEx.Helpers;
using Microsoft.AspNetCore.Mvc;
using Mrc.Application;
using Mrc.Data;
using Mrc.Entity;

namespace MrcWeb.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Login]
    [Permission("system.role")]
    public class RoleController :BaseController
    {
        private readonly ISysRoleService _sysRoleService;
        private readonly ISysRolePermissionService _sysRolePermissionService;
        private readonly ISysPermissionService _sysPermissionService;
        public RoleController(ISysRoleService sysRoleService,
            ISysRolePermissionService sysRolePermissionService,
            ISysPermissionService sysPermissionService)
        {
            this._sysPermissionService = sysPermissionService;
            this._sysRolePermissionService = sysRolePermissionService;
            this._sysRoleService = sysRoleService;
        }
        // GET: SystemManage/Role
        public ActionResult Index()
        {
            return View();
        }
        [Permission("system.role.add")]
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult Update()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Models(string keyword)
        {
            List<SysRole> data = _sysRoleService.GetRoles(keyword);
            return Json(data.ToLayuiTable(1,data.Count));
        }

        [Permission("system.role.add")]
        [HttpPost]
        public ActionResult Add(SysRole input)
        {
            input.Id = IdHelper.CreateStringSnowflakeId();      
            input.CreateUserId = this.CurrentSession.UserId;
            _sysRoleService.Add(input);
            return Json(AjaxResult.CreateResult(ResultStatus.OK,"添加成功"));
        }
        [Permission("system.role.update")]
        [HttpPost]
        public ActionResult Update(SysRole input)
        {
            var role= _sysRoleService.Single(x => x.Id == input.Id);
            if(role == null) return Json(AjaxResult.CreateResult(ResultStatus.Failed, "查找角色失败"));
            role.Name = input.Name;
            role.Description = input.Description;
            role.IsEnabled = input.IsEnabled;
            _sysRoleService.Update(role);
            return Json(AjaxResult.CreateResult(ResultStatus.OK, "更新成功"));
        }

        [Permission("system.role.update")]
        [HttpPost]
        public ActionResult UpdateStatus(string id,bool isEnable)
        {
            var role = _sysRoleService.Single(x => x.Id == id);
            if(role==null) return Json(AjaxResult.CreateResult(ResultStatus.Failed, "查找角色失败"));
            role.IsEnabled = isEnable;
            _sysRoleService.Update(role);
            return Json(AjaxResult.CreateResult(ResultStatus.OK, "更新成功"));
        }

        [Permission("system.role.update")]
        public JsonResult GetData(string id)
        {
            return Json(AjaxResult.CreateResult<SysRole>(ResultStatus.OK, "OK", _sysRoleService.Single(x => x.Id == id)));
        }

        [Permission("system.role.delete")]
        [HttpPost]
        public ActionResult Delete(string[] ids)
        {
            foreach (var item in ids)
            {
                _sysRoleService.Delete(item, this.CurrentSession.UserId);
            }
            return Json(AjaxResult.CreateResult(ResultStatus.OK,"删除成功"));
        }
        public ActionResult GetRolePermissions(string id)
        {
            List<SysPermission> sysPermissionList = _sysPermissionService.GetList();
            List<SysRolePermission> authorizedata = new List<SysRolePermission>();
            if (!string.IsNullOrEmpty(id))
            {
                authorizedata = _sysRoleService.GetPermissions(id);
            }
            List<string> permissions = authorizedata.Select(x => x.PermissionId).ToList();

            DtreeHelper<SysPermission> dtreeHelper = new DtreeHelper<SysPermission>(sysPermissionList,EnumExtension.GetDic<PermissionType>(),permissions);
            var datas = dtreeHelper.GetData();
            dtreeHelper = null;
            return Json(new { status = new { code = ResultStatus.OK, message = "ok" }, data = datas });
        }

        [Permission("system.role.set_permission")]
        [HttpPost]
        public ActionResult SetPermission(string id, string permissions)
        {
            List<string> permissionList = permissions.Split(',').Distinct().ToList();
            _sysRoleService.SetPermission(id, permissionList);
            return Json(AjaxResult.CreateResult(ResultStatus.OK,"设置成功"));
        }
    }

}
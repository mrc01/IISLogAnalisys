﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MrcWeb.Controllers;
using Microsoft.AspNetCore.Mvc;
using Mrc.Entity;
using Mrc.Application;
using DotNetEx;

namespace MrcWeb.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Login]
    public class UserController : BaseController
    {
        private readonly ISysUserService _sysUserService;
        private readonly ISysRoleService _sysRoleService;
        private readonly ISysPostService _sysPostService;
        private readonly ISysOrgService  _sysOrgService;
        public UserController(ISysUserService sysUserService,ISysPostService sysPostService,ISysRoleService sysRoleService,ISysOrgService sysOrgService)
        {
            this._sysUserService = sysUserService;
            this._sysRoleService = sysRoleService;
            this._sysPostService = sysPostService;
            this._sysOrgService = sysOrgService;
        }
        public IActionResult Index()
        {
            ViewBag.orgName = JsonHelper.Serialize(_sysOrgService.List().Select(x => new { id = x.Id, Name = x.Name }).ToList().ToDictionary(z => z.id, z => z.Name));
            ViewBag.postName = JsonHelper.Serialize(_sysPostService.List().Select(x => new { id = x.Id, Name = x.Name }).ToList().ToDictionary(z => z.id, z => z.Name));
            ViewBag.roleName = JsonHelper.Serialize(_sysRoleService.List().Select(x => new { id = x.Id, Name = x.Name }).ToList().ToDictionary(z => z.id, z => z.Name));
            return View();
        }
        public IActionResult Add()
        {
            ViewBag.orgs =_sysOrgService.List().ToList();
            ViewBag.posts = _sysPostService.List().ToList();
            ViewBag.roles = _sysRoleService.List().ToList();
            return View();
        }
        public IActionResult Update()
        {
            ViewBag.orgs = _sysOrgService.List().ToList();
            ViewBag.posts = _sysPostService.List().ToList();
            ViewBag.roles = _sysRoleService.List().ToList();
            var id=HttpContext.Request.Query["Id"].ToString();
            var model= _sysUserService.Single(id);
            return View(model);
        }

        public IActionResult UpdatePassword()
        {
            var id = HttpContext.Request.Query["Id"].ToString();
            var model = _sysUserService.Single(id);
            return View(model);
        }

        [HttpGet]
        public ActionResult Models(string keyword)
        {
           var data = _sysUserService.List(x=>x.Id!=string.Empty).ToLayuiTable(1,100);
            return Json(data);
        }


        [HttpPost]
        [Permission("system.user.add")]
        public ActionResult Add(AddUserInput input)
        {
            _sysUserService.Add(input);            
            return Json(AjaxResult.CreateResult("创建成功"));
        }

        [HttpPost]
        [Permission("system.user.update")]
        public ActionResult Update(AddUserInput input)
        {
            _sysUserService.Add(input);
            return Json(AjaxResult.CreateResult("创建成功"));
        }

        [HttpPost]
        [Permission("system.user.revise_password")]
        public ActionResult UpdatePassword(UpdateUserPwdInput input)
        {
            _sysUserService.UpdatePassword(input);
            return Json(AjaxResult.CreateResult("修改成功"));
        }
    }
}
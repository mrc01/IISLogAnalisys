﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MrcWeb.Models;
using Mrc.Entity;
using Mrc.Application;
using Mrc.IISLog;
using Mrc.Data;
using Mrc.RabbitMq.Helper;

namespace MrcWeb.Controllers
{
    
   
    public class HomeController : Controller
    {
        private readonly ISysUserLogOnService _userService;
        private readonly ISysFilterIPService _sysFilterIPService;

        public HomeController(ISysUserLogOnService userService,ISysFilterIPService sysFilterIPService)
        {
            _userService = userService;
            _sysFilterIPService = sysFilterIPService;
        }
        public async Task<IActionResult> Index()
        {
           //return View();
           var result=await IISManager.SetLogs();
           return Redirect("/Admin/Home");//入口
        }

        public ActionResult PageNoAuth()
        {
            return View();
        }


        public ActionResult send(string msg,string rkey)
        {
            try
            {
            // RabbitMQPushHelper.PushMessage(msg, rkey);
            }
            catch (Exception ex)
            {
            }
            return Json("");      
        }

        [Login]
        public JsonResult testServeice()
        {       
            var list=_userService.List(x => x.UserSecretkey == "111").ToList();
            return Json(AjaxResult.CreateResult<List<SysUserLogOn>>(ResultStatus.OK,list));
        }


        [HttpGet]
        public async Task<JsonResult> getData()
        {
           var e1 =await _sysFilterIPService.GetListAsync();
           var e2 =await _sysFilterIPService.GetListAsync();
           var e3 =await _sysFilterIPService.GetListAsync();
           var e4 =await _sysFilterIPService.GetListAsync();
           var e5 =await _sysFilterIPService.GetListAsync();
           var e6 =await _sysFilterIPService.GetListAsync();
           var e7 =await _sysFilterIPService.GetListAsync();
           var e8 =await _sysFilterIPService.GetListAsync();
           var e9 =await _sysFilterIPService.GetListAsync();
            return Json("");
        }


        public async Task<JsonResult> GetLogs()
        {
            return Json(await IISManager.GetAllIISLogsName());
        }

        public async Task<JsonResult> GetLogIps(string name)
        {
            return Json(await IISManager.GetLogIps(name));
        }

        public ActionResult LogIndex()
        {
            return View();
        }
        public ActionResult main()
        {
            return View();
        }

        public async Task<JsonResult> GetOneLog(string name = "", int pageNumber = 1, int pageSize = 20)
        {
            return Json(await IISManager.GetLogs(name,pageNumber, pageSize));
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

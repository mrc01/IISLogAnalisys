﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mrc.Application;
using Mrc.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Mrc.IISLog;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace MrcWeb.Controllers
{
    public class LoginController : BaseController
    {
        private ILogger<LoginController> _logger;
        private readonly IAccountService _accountService;
        public LoginController(IAccountService accountService,ILogger<LoginController> logger)
        {
            _accountService = accountService;
            _logger = logger;

        }
        public IActionResult Index()
        {
            //_logger.LogError("hello world");
            //var list= IISHelper.GetSiteList();
            //var listName = new List<string>();
            //foreach (var item in list)
            //{
            //    listName.Add(item.Name);
            //}
            return View();
        }

        [HttpPost]
        public JsonResult loginpost(string user,string passwd)
        {
            AdminSession session = new AdminSession();
            SysUser userModel = null;
            string msg = string.Empty;
            if (!_accountService.CheckLogin(user, passwd, out userModel, out msg))
            {
              //  this.CreateService<ISysLogAppService>().LogAsync(null, null, ip, LogType.Login, moduleName, false, "用户[{0}]登录失败：{1}".ToFormat(loginName, msg));
                return Json(AjaxResult.CreateResult(ResultStatus.Failed, msg));
            }
            session.UserId = userModel.Id;
            session.AccountName = user;
            session.Name = user;
            session.LoginIP = this.HttpContext.GetClientIP();
            session.IsAdmin = userModel.IsAdmin();
            this.CurrentSession = session;
            return Json(AjaxResult.CreateResult(ResultStatus.OK,"登录成功"));
        }
    }
}

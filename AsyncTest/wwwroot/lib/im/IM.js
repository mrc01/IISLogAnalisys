    //哈希表
    function HashTable() {
        var size = 0;
        var entry = new Object();
        this.add = function (key, value) {
            if (!this.containsKey(key)) {
                size++;
            }
            entry[key] = value;
        }
        this.getValue = function (key) {
            return this.containsKey(key) ? entry[key] : null;
        }
        this.remove = function (key) {
            if (this.containsKey(key) && (delete entry[key])) {
                size--;
            }
        }
        this.containsKey = function (key) {
            return (key in entry);
        }
        this.containsValue = function (value) {
            for (var prop in entry) {
                if (entry[prop] == value) {
                    return true;
                }
            }
            return false;
        }
        this.getValues = function () {
            var values = new Array();
            for (var prop in entry) {
                values.push(entry[prop]);
            }
            return values;
        }
        this.getKeys = function () {
            var keys = new Array();
            for (var prop in entry) {
                keys.push(prop);
            }
            return keys;
        }
        this.getSize = function () {
            return size;
        }
        this.clear = function () {
            size = 0;
            entry = new Object();
        }
    }
    
    // 函数名	            说明	                   返回值
    // add(key,value)   	添加项	                   无
    // getValue(key)	    根据key取值	               object
    // remove(key)	        根据key删除一项             无
    // containsKey(key)	    是否包含某个key         	bool
    // containsValue(value)	是否包含某个值	            bool
    // getValues()	        获取所有的值的数组      	array
    // getKeys()	        获取所有的key的数组	        array
    // getSize()	        获取项总数	               int
    // clear()	            清空哈希表	               无

  function stompClient() 
  {
        this.hashTable = new HashTable();
        this.minTimeStamp = 0; 
        this.maxTimeStamp = 0; 
        this.isFrist=true;
        this.setData=function(data)
        {
            var time=parseInt(data.time);
            if(time>this.maxTimeStamp)
            {
                this.maxTimeStamp=data.time;
            }
            else if(data.time<this.minTimeStamp||this.isFrist)
            {
                this.minTimeStamp=data.time;
            }
            if(this.isFrist)
            {
                this.minTimeStamp=data.time; 
                this.isFrist=false;
            }  
            this.hashTable.add(data.time,data);
        }
        this.init = function(clientUrl,name,callBack) {
            //声明个Stompjs客户端
            var client = Stomp.client(clientUrl);
            //监听连接事件 
            var on_connect = function (x) {
                id = client.subscribe("/topic/"+name, function (respData) 
                {
                    callBack(respData.body);                   
                });
            };
            var on_error = function () {
                console.log('error');
            };
            //连接MQ
            client.connect("xman","xman01111",on_connect, on_error, '/');
        }
    }

   
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Mrc.Entity
{
    /// <summary>
    /// bug等级
    /// </summary>
    public enum ProjectBugSeverityLevelEnums
    {
        [Description("致命Bug")]
        致命Bug = 0,
        [Description("严重Bug")]
        严重Bug = 2,
        [Description("一般Bug")]
        一般Bug = 4,
    }


}

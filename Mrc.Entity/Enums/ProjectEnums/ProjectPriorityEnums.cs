﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Mrc.Entity
{
    public enum ProjectPriorityEnums
    {
        [Description("低")]
        低 = 0,
        [Description("中")]
        中 = 2,
        [Description("高")]
        高 = 4,
        [Description("极高")]
        极高 = 6,
    }
}

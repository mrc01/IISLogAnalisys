﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Mrc.Entity
{
    public enum ProjectBugTypeEnum
    {
        [Description("功能错误")]
        功能错误 = 0,
        [Description("设计缺陷")]
        设计缺陷 = 1,
        [Description("界面优化")]
        界面优化 = 2,
        [Description("性能问题")]
        性能问题 = 3,
        [Description("配置相关")]
        配置相关 = 4,
        [Description("安装部署")]
        安装部署 = 5,
        [Description("安全相关")]
        安全相关 = 6,
        [Description("标准规范")]
        标准规范 = 7,
        [Description("测试脚本")]
        测试脚本 = 8,
        [Description("其他")]
        其他 = 9,
    }
}

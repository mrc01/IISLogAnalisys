﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Mrc.Entity
{
    public enum ProjectTaskTypeEnum
    {
        [Description("开发")]
        开发 = 0,
        [Description("测试")]
        测试 = 1,
        [Description("线上反馈")]
        线上反馈 = 2,
    }
}

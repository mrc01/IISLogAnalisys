﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Mrc.Entity
{
    public enum ProjectBugStatusEnum
    {
        [Description("待解决")]
        待解决 = 0,
        [Description("已解决")]
        已解决 = 1,
        [Description("重现未解决")]
        重现未解决 = 2,
    }
}

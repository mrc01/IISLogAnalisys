﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Mrc.Entity
{
    public enum ProjectTaskStatusEnum
    {
        [Description("开发中")]
        开发中 = 0,
        [Description("测试中")]
        测试中 = 1,
        [Description("已完成")]
        已完成 = 2,
    }
}

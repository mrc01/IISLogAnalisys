﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Entity.Enums.ProjectEnums 
* 项目描述 ： 
* 类 名 称 ：ProjectStatus 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Entity.Enums.ProjectEnums
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/3/4 23:42:33 
* 更新时间 ：2019/3/4 23:42:33 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Entity
{
    public enum ProjectStatusEnum
    {
        待下发=0,
        对接中=2,
        开发中 = 4,
        测试中 = 6,
        已上线=8,
        升级中=10,
    }
}

﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Entity.Models.ProjectModels 
* 项目描述 ： 
* 类 名 称 ：AddUserProjectDemand 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Entity.Models.ProjectModels
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/3/9 10:50:04 
* 更新时间 ：2019/3/9 10:50:04 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Entity
{
    public class AddUpdateUserProjectDemand:UserProjectDemand
    {
        public string userIds { get; set; }
    }
}

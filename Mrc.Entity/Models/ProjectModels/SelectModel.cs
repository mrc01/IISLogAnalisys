﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Entity.Models.ProjectModels 
* 项目描述 ： 
* 类 名 称 ：ProjectTypeSelectModel 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Entity.Models.ProjectModels
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/3/5 21:19:14 
* 更新时间 ：2019/3/5 21:19:14 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Entity.Models
{
    public class SelectModel
    {
        public string name { get; set;}
        public string value { get; set; }
        public string selected { get; set; }
        public string disabled { get; set; }
    }
}

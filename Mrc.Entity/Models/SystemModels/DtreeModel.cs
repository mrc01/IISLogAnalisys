﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Entity.Models 
* 项目描述 ： 
* 类 名 称 ：DtreeModel 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Entity.Models
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/2/24 23:42:20 
* 更新时间 ：2019/2/24 23:42:20 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Entity
{

    /** 树类*/
    public class DTree
    {
        /** 节点ID*/
        public string id { get; set; }

        /** 上级节点ID*/
        public string parentId { get; set; }

        /** 节点名称*/
        public string title { get; set; }

        /** 层级*/
        public string level { get; set; }

        /** 是否最后一级节点*/
        public bool isLast { get; set; }

        public int sortCode { get; set; }
        /** 自定义图标class*/
        public string iconClass { get; set; }

        /** 表示用户自定义需要存储在树节点中的数据*/
        public object basicData { get; set; }

        /** 复选框集合*/
        public List<CheckArr> checkArr = new List<CheckArr>();

        /** 子节点集合*/
        public List<DTree> children = new List<DTree>();

        public DTree() { }

        public bool isShow { get; set;}

        public DTree(string id, string parentId, string title, string iconClass,
                object basicData, List<CheckArr> checkArr)
        {
            this.id = id;
            this.parentId = parentId;
            this.title = title;
            this.iconClass = iconClass;
            this.basicData = basicData;
            this.checkArr = checkArr;
        }

        public DTree(string id, string parentId, string title, string level,
                bool isLast, string iconClass, object basicData,
                List<CheckArr> checkArr, List<DTree> children)
        {
            this.id = id;
            this.parentId = parentId;
            this.title = title;
            this.level = level;
            this.isLast = isLast;
            this.iconClass = iconClass;
            this.basicData = basicData;
            this.checkArr = checkArr;
            this.children = children;
        }

        public string toString()
        {
            return $"DTree [id={id}, parentId={parentId}, title={title}, level={level}, isLast={isLast},iconClass={iconClass}, basicData={basicData}, checkArr={checkArr}, children={children}]";
        }

    }


    /** 复选框设计类*/
    public class CheckArr
    {
        /** 复选框标记*/
        public string type { get; set; }

        /** 复选框是否选中*/
        public string isChecked { get; set; }

        public CheckArr() { }

        public CheckArr(string type, string isChecked)
        {
            this.type = type;
            this.isChecked = isChecked;
        }
      
        public string toString()
        {
            return $"CheckArr [type={type}, isChecked={isChecked}]";
        }

    }

}

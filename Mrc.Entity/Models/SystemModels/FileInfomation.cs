﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Entity
{
    public class FileInfomation
    {
        public string FileName { get; set; }
        public string FileFullName { get; set; }
        public decimal FileSize { get; set; }
        public DateTime CreateTime { get; set; }
        public string FilePath { get; set; }
        public bool isRead { get; set; } = false;
    }
}

﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Entity.Models 
* 项目描述 ： 
* 类 名 称 ：PermissionModel 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Entity.Models
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/2/24 20:40:00 
* 更新时间 ：2019/2/24 20:40:00 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Entity
{
    public class PermissionModel
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public static PermissionModel Create(SysPermission entity, int level = 0)
        {
            PermissionModel ret = new PermissionModel() { Id = entity.Id, Name = entity.Name };

            for (int i = 0; i < level; i++)
            {
                ret.Name = "　" + ret.Name;
            }

            return ret;
        }
    }
}

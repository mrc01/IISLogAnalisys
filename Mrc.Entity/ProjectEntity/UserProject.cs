﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mrc.Entity
{ 
    public class UserProject:BaseEntity
    {
        public string Name { get; set; }
        public int? Status { get; set; }
        public string TypeId { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public decimal? Amount { get; set;}
        public string TemplateProjectID { get; set; }
        public string TemplateProjectName { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateUserId { get; set; }        
        public string ProjectUrl { get; set; }      
    }
}

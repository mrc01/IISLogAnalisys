﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Entity.ProjectEntity 
* 项目描述 ： 
* 类 名 称 ：UserProjectTask 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Entity.ProjectEntity
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/3/6 22:35:29 
* 更新时间 ：2019/3/6 22:35:29 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Mrc.Entity
{
    public class UserProjectTask : BaseEntity
    {
        public string Name { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public DateTime? PlanStarTime { get; set; }
        public DateTime? PlanFinishTime { get; set; }
        public DateTime? ActualStartTime { get; set; }
        public DateTime? ActualFinishTime { get; set; }
        public int? Status { get; set; }
        public string ExecuteUser { get; set; }
        public string ExecuteUId { get; set; }
        public decimal Process { get; set; }
        public int? Priority { get; set; }
        public int? Type { get; set; }
        public string Description { get; set; }
        public decimal Duration { get; set; }
        public string DemandId { get; set; }
        public string DemandName { get; set; }
    }

    public class AddProjectTaskDto
    {
        public string Name { get; set; }
        public string ProjectId { get; set; }
        public DateTime? PlanStarTime { get; set; }
        public DateTime? PlanFinishTime { get; set; }
        public int? Priority { get; set; }
        public string ExecuteUId { get; set; }
        public string DemandId { get; set; }
        public string Description { get; set; }
        public string userIds { get; set; }
        public int? Type { get; set; }
    }

    public class UpdateProjectTaskDto
    {
        public string Id { get; set; }
        public string ProjectId { get; set; }
        public string Name { get; set; }

        public DateTime? ActualStartTime { get; set; }
        public DateTime? ActualFinishTime { get; set; }
        public DateTime? PlanStarTime { get; set; }
        public DateTime? PlanFinishTime { get; set; }
        public int? Priority { get; set; }
        public string ExecuteUId { get; set; }
        public string DemandId { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public decimal Process { get; set; }
        public int? Type { get; set; }
        public string userIds { get; set; }
    }
 
    public class ProjectTaskListModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ProjectName { get; set; }
        public string DemandName { get; set; }
        public decimal Process { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; }
        public DateTime? PlanStarTime { get; set; }
        public DateTime? PlanFinishTime { get; set; }
        public string Description { get; set; }
    }
}

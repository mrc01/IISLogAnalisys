﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Entity.ProjectEntity 
* 项目描述 ： 
* 类 名 称 ：UserProjectTask 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Entity.ProjectEntity
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/3/6 22:35:29 
* 更新时间 ：2019/3/6 22:35:29 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Mrc.Entity
{
    public class UserProjectBug : BaseEntity
    {

        public DateTime? CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public string ExecuteUser { get; set; }
        public string ExecuteUId { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string DemandId { get; set; }
        public string DemandName { get; set; }
        public string TaskId { get; set; }
        public string TaskName { get; set; }
        public int? Type { get; set; }
        public int? Status { get; set; }
        public int? Priority { get; set; }
        public int? SeverityLevel { get; set; }
        public DateTime? FinishTime { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ReappearStep { get; set; }
        public string Remark { get; set; }     
    }

    public class AddProjectBugDto
    {
        public string ExecuteUId { get; set; }
        public string ProjectId { get; set; }
        public string TaskId { get; set; }
        public string DemandId { get; set; }
        public int? Type { get; set; }
        public int? Priority { get; set; }
        public int? SeverityLevel { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ReappearStep { get; set; }
       // public string Remark { get; set; }

    }
    public class UpdateProjectBugDto:AddProjectBugDto
    {
        public string Id { get; set; }
        public int? Status { get; set; }
    }
    public class ProjectBugModels
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ExecuteUser { get; set; }
        public string ProjectName { get; set; }
        public string DemandName { get; set; }
        public string TaskName { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string Priority { get; set; }
        public string SeverityLevel { get; set; }
        public string Remark { get; set; }

    }

}

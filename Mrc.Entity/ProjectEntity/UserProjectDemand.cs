﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Entity
{
    public class UserProjectDemand : BaseEntity
    {
        public DateTime? CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public string ExecuteUser { get; set; }
        public string ExecuteUId { get; set; }
        public string Name { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int? Status { get; set; }
        public int? Priority { get; set; }
        public int Type { get; set; }
        public DateTime? Endtime { get; set; }
        public decimal? Process { get; set; }
        public string Description { get; set; }    
    }

    public class UserProjectDemandDto
    {
        public string Id { get; set; }
        public DateTime? Endtime { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string ExecuteUser { get; set; }
        public string ProjectName { get; set; }
        public string Priority { get; set; }
        public string UserNames { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }

    }
}

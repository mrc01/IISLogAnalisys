﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mrc.Entity
{
    public class UserProjectMember: BaseEntity
    {
        public DateTime? CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public bool IsEnabled { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string UserHeadImg { get; set; }
        public string TaskId { get; set; }
        public string ProjectDemandId { get; set; }        
        public string Type { get; set; }
        public string Description { get; set; }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mrc.Entity
{
    public class SysDBBackup: BaseEntity
    {
        public DateTime CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public decimal? Size { get; set; }
        public string Description { get; set; }
        public int? RestoreTimes { get; set; }
        public DateTime? LastModifyTime { get; set; }
        public string LastModifyUid { get; set; }     
    }
    public class UpdateSysDBBackupDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class SysDBBackuModels
    {
        public string Id { get; set; }
        public string CreateName { get; set; }
        public string Name { get; set; }
        public decimal? Size { get; set; }
        public string Description { get; set; }
        public int? RestoreTimes { get; set; }
        public DateTime CreateTime { get; set; }

    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mrc.Entity
{

    public class SysUserPermission:BaseEntity
    {
        public string UserId { get; set; }
        public string PermissionId { get; set; }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mrc.Entity
{

    public class SysOrgType:BaseEntity
    {
        public string Name { get; set; }
        public int? ParentId { get; set; }
    }
}

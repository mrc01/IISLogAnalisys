﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Entity.SystemEntity 
* 项目描述 ： 
* 类 名 称 ：SysFilterIP 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Entity.SystemEntity
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/2/20 9:36:17 
* 更新时间 ：2019/2/20 9:36:17 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Entity
{
    public class SysFilterIP:BaseEntity
    {
        public int IpType { get; set; }
        public string StartIP { get; set; }
        public string EndIP { get; set; }
        public int SortCode { get; set; }
        public bool IsEnable { get; set; }
        public string Description { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public DateTime? LastModifyTime { get; set; }
        public string LastModifyUserId { get; set; }

    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mrc.Entity
{
 
    public class SysUserPost:BaseEntity
    {
        public string UserId { get; set; }
        public string PostId { get; set; }
    }
}

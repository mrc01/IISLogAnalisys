﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mrc.Entity
{

    public class SysUserRole:BaseEntity
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
    }
}

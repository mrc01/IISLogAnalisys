﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mrc.Entity
{

    public class SysPost:BaseEntity
    {
        public string Name { get; set; }
        public string OrgId { get; set; }
        public string Description { get; set; }
        public DateTime CreateTime { get; set; }
    }
}

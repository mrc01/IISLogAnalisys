﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mrc.Entity
{

    public class SysOrgPermission:BaseEntity
    {

        public string OrgId { get; set; }
        public string PermissionId { get; set; }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mrc.Entity
{

    public class SysRolePermission:BaseEntity
    {
        public string RoleId { get; set; }
        public string PermissionId { get; set; }
    }
}

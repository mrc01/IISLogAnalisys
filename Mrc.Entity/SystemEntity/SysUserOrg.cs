﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mrc.Entity
{

    public class SysUserOrg:BaseEntity
    {
        public string UserId { get; set; }
        public string OrgId { get; set; }
        public bool DisablePermission { get; set; }
        public SysOrg Org { get; set; }
    }
}

﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Entity.SystemEntity 
* 项目描述 ： 
* 类 名 称 ：SysFilterIP 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Entity.SystemEntity
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/2/20 9:36:17 
* 更新时间 ：2019/2/20 9:36:17 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Entity
{
    public class SysC2CIM : BaseEntity
    {
        public string FromUid { get; set; }
        public string ToUid { get; set; }
        public string FromName { get; set; }
        public string ToName { get; set; }
        public string Msg { get; set; }
        public int MsgType { get; set; }
        public long Time { get; set; }
        public bool IsRead { get; set; }

    }
}

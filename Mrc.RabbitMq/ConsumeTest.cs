﻿using Chloe.SqlServer;
using Mrc.Data;
using Mrc.Entity;
using Mrc.RabbitMq.Helper;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mrc.RabbitMq
{
    public class ConsumeTest
    {
        public async static void StartRun()
        {
            await Task.Factory.StartNew(() =>
            {
                var factory = new ConnectionFactory()
                {
                    HostName = "192.168.1.228",
                    Port = 5672,
                    UserName = "xman",
                    Password = "xman01111"
                };
                var Xname = "fanout_test1";
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: Xname, type: ExchangeType.fanout);
                    var queueName = channel.QueueDeclare().QueueName;
                    channel.QueueBind(queue: queueName, exchange: Xname, routingKey: "");
                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        var message = $"xname:{Xname} message:{Encoding.UTF8.GetString(ea.Body)}";
                        Task.Run(() =>
                        {
                            using (MsSqlContext dbContext = new MsSqlContext("Data Source =192.168.1.228;Initial Catalog =IISLogAnalisys;Persist Security Info=True;User ID=sa;Password=123"))
                            {
                                dbContext.Insert<RabbitMQMessage>(new RabbitMQMessage { Body = message });
                               // RabbitMQPushHelper.PushMessage(message, "mq", "topic");
                                using (var channel2 = connection.CreateModel())
                                {
                                    //绑定交换器
                                    channel2.ExchangeDeclare(exchange: "topic", type: "topic");
                                    var body = Encoding.UTF8.GetBytes(message);
                                    //对指定routingkey发送内容
                                    channel2.BasicPublish(exchange: "topic", routingKey: "mq", basicProperties: null, body: body);
                                }
                            }
                        });
                    };
                    channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);
                    //Console.ReadLine();
                }
               // Console.ReadLine();
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.RabbitMq
{
    public class ExchangeType
    {
        public static string fanout { get; } = "fanout";
        public static string direct { get; } = "direct";
        public static string topic { get; } = "topic";
        public static string headers { get; } = "headers";

    }
}

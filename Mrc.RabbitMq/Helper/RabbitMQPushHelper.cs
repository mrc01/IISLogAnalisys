﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.RabbitMq.Helper
{
    public class RabbitMQPushHelper
    {
        private static IConnection connection;
        public static IConnection GetConection()
        {
           if(connection==null)
           {
                var factory = new ConnectionFactory()
                {
                    HostName = "192.168.1.29",
                    Port = 5672,
                    UserName = "xman",
                    Password = "xman01111"
                };
                connection =factory.CreateConnection();
           }
            return connection;
        }
        public static void PushMessage(string msgBody,string routingKey,string exchange,string type)
        {
            using (var channel = GetConection().CreateModel())
            {
                //绑定交换器
                channel.ExchangeDeclare(exchange: exchange, type:type,durable:true,autoDelete:false,arguments:null);
                var body = Encoding.UTF8.GetBytes(msgBody);
                //对指定routingkey发送内容
                channel.BasicPublish(exchange:exchange,routingKey:routingKey, basicProperties: null,body: body);
            }
        }
    }
}

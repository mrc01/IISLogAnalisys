﻿using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface ISysLogService : IServiceBase<SysLog>, IMrcService
    {

    }
    public class SysLogService : ServiceBase<SysLog>, ISysLogService
    {
        public SysLogService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}


﻿using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Mrc.Application
{
    public interface ISysFilterIPService : IServiceBase<SysFilterIP>, IMrcService
    {
        Task<List<SysFilterIP>> GetListAsync();
    }
    public class SysFilterIPService : ServiceBase<SysFilterIP>, ISysFilterIPService
    {
        public SysFilterIPService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
        public Task<List<SysFilterIP>> GetListAsync()
        {
            return Task.Run(() =>
            {
                using (var context = DbContextHelper.GetDBContext())
                {
                   return context.Query<SysFilterIP>().ToList();
                }
            });           
        }
    }
}


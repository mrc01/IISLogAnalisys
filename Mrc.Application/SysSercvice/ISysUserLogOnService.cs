﻿using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface ISysUserLogOnService : IServiceBase<SysUserLogOn>,IMrcService
    {

    }
    public class SysUserLogOnService : ServiceBase<SysUserLogOn>, ISysUserLogOnService
    {
        public SysUserLogOnService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}

﻿


using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface ISysUserPostService : IServiceBase<SysUserPost>, IMrcService
    {

    }
    public class SysUserPostService : ServiceBase<SysUserPost>, ISysUserPostService
    {
        public SysUserPostService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}

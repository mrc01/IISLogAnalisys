﻿

using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface ISysUserOrgService : IServiceBase<SysUserOrg>, IMrcService
    {

    }
    public class SysUserOrgService : ServiceBase<SysUserOrg>, ISysUserOrgService
    {
        public SysUserOrgService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}

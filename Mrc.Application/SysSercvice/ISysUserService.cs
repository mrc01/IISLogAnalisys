﻿
using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;
using System.Linq;
using DotNetEx;
using DotNetEx.ExceptionEx;

namespace Mrc.Application
{
    public interface ISysUserService : IServiceBase<SysUser>, IMrcService
    {
        void Add(AddUserInput input);
        void Update(AddUserInput input);
        
        void UpdatePassword(UpdateUserPwdInput input);

        List<SysUserPermission> GetPermissions(string id);
        void SetPermission(string id, List<string> permissionList);
        List<SysPermission> GetUserPermissions(string id);
        List<string> GetUserPermits(string id);
    }
    public class SysUserService : ServiceBase<SysUser>, ISysUserService
    {
        public SysUserService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }

        public List<SysUserPermission> GetPermissions(string id)
        {
            return this.DbContext.Query<SysUserPermission>().Where(t => t.UserId == id).ToList();
        }
        public void SetPermission(string id, List<string> permissionList)
        {
            id.IsNotNullOrEmpty();
            List<SysUserPermission> rolePermissions = permissionList.Select(a => new SysUserPermission()
            {
                Id = IdHelper.CreateStringSnowflakeId(),
                UserId = id,
                PermissionId = a
            }).ToList();

            this.DbContext.DoWithTransaction(() =>
            {
                this.DbContext.Delete<SysUserPermission>(a => a.UserId == id);
                this.DbContext.InsertRange(rolePermissions);
            });
        }
        public List<SysPermission> GetUserPermissions(string id)
        {
            List<SysPermission> ret = new List<SysPermission>();

            List<string> userPermissionIds = this.DbContext.Query<SysUserPermission>().Where(a => a.UserId == id).Select(a => a.PermissionId).ToList();

            List<string> rolePermissionIds = this.DbContext.JoinQuery<SysRolePermission, SysRole, SysUserRole>((rolePermission, role, userRole) => new object[] {
                JoinType.InnerJoin,rolePermission.RoleId==role.Id,
                JoinType.InnerJoin,role.Id==userRole.RoleId
            })
            .Where((rolePermission, role, userRole) => userRole.UserId == id)
            .Select((rolePermission, role, userRole) => rolePermission.PermissionId).ToList();

            List<string> orgPermissionIds = this.DbContext.JoinQuery<SysOrgPermission, SysOrg, SysUserOrg>((orgPermission, org, userOrg) => new object[] {
                JoinType.InnerJoin,orgPermission.OrgId==org.Id,
                JoinType.InnerJoin,org.Id==userOrg.OrgId
            })
            .Where((orgPermission, org, userOrg) => userOrg.UserId == id && userOrg.DisablePermission == false)
            .Select((orgPermission, org, userOrg) => orgPermission.PermissionId).ToList();

            List<string> permissionIds = userPermissionIds.Concat(rolePermissionIds).Concat(orgPermissionIds).Distinct().ToList();

            ret = this.DbContext.Query<SysPermission>().Where(a => permissionIds.Contains(a.Id)).ToList();

            return ret;
        }
        public List<string> GetUserPermits(string id)
        {
            var ret = this.GetUserPermissions(id).Where(a => a.Code.IsNotNullOrEmpty()).Select(a => a.Code).ToList();
            return ret;
        }
        public void Add(AddUserInput input)
        {
            if (input.AccountName.IsNullOrEmpty() && input.MobilePhone.IsNullOrEmpty() && input.Email.IsNullOrEmpty())
            {
                throw new InvalidInputException("用户名/手机号码/邮箱至少填一个");
            }
            string accountName = null;
            if (input.AccountName.IsNotNullOrEmpty())
            {
                accountName = input.AccountName.ToLower();
                bool exists = this.DbContext.Query<SysUser>().Where(a => a.AccountName == accountName).Any();
                if (exists)
                    throw new InvalidInputException($"用户名[{input.AccountName}]已存在");
            }
            string mobilePhone = null;
            if (input.MobilePhone.IsNotNullOrEmpty())
            {
                mobilePhone = input.MobilePhone;
                if (MrcUtils.IsMobilePhone(mobilePhone) == false)
                    throw new InvalidInputException("请输入正确的手机号码");

                bool exists = this.DbContext.Query<SysUser>().Where(a => a.MobilePhone == mobilePhone).Any();
                if (exists)
                    throw new InvalidInputException($"手机号码[{mobilePhone}]已存在");
            }
            string email = null;
            if (input.Email.IsNotNullOrEmpty())
            {
                email = input.Email.ToLower();
                if (MrcUtils.IsEmail(email) == false)
                    throw new InvalidInputException("请输入正确的邮箱地址");

                bool exists = this.DbContext.Query<SysUser>().Where(a => a.Email == email).Any();
                if (exists)
                    throw new InvalidInputException($"邮箱地址[{input.Email}]已存在");
            }

            SysUser user =new SysUser();
            user.CreateTime = DateTime.Now;
            user.IsDeleted = false;
            user.AccountName = accountName;
            user.Name = input.Name;
            user.Gender = input.Gender;
            user.MobilePhone = mobilePhone;
            user.Birthday = input.Birthday;
            user.WeChat = input.WeChat;
            user.Email = email;
            user.Description = input.Description;
            user.State = AccountState.Normal;
            user.Id = IdHelper.CreateStringSnowflakeId();
            string userSecretkey = UserHelper.GenUserSecretkey();
            string encryptedPassword = EncryptHelper.EncryptMD5Password(input.Password, userSecretkey);

            SysUserLogOn logOnEntity = new SysUserLogOn();
            logOnEntity.Id = IdHelper.CreateStringSnowflakeId();
            logOnEntity.UserId = user.Id;
            logOnEntity.UserSecretkey = userSecretkey;
            logOnEntity.UserPassword = encryptedPassword;

            List<string> roleIds = (input.RoleIds??"").Split(',').ToList();
            List<SysUserRole> userRoles = roleIds.Select(a =>
            {
                return new SysUserRole()
                {
                    Id = IdHelper.CreateStringSnowflakeId(),
                    UserId = user.Id,
                    RoleId = a,
                };
            }).ToList();
            user.RoleIds = string.Join(",", roleIds);
            List<string> orgIds = (input.OrgIds??"").Split(',').ToList();
            List<SysUserOrg> userOrgs = orgIds.Select(a =>
            {
                return new SysUserOrg()
                {
                    Id = IdHelper.CreateStringSnowflakeId(),
                    UserId = user.Id,
                    OrgId = a,
                    DisablePermission = false
                };
            }).ToList();

            user.OrgIds = string.Join(",", orgIds);
            List<string> postIds = input.PostIds.Split(',').ToList();
            List<SysUserPost> userPosts = postIds.Select(a =>
            {
                return new SysUserPost()
                {
                    Id = IdHelper.CreateStringSnowflakeId(),
                    UserId = user.Id,
                    PostId = a
                };
            }).ToList();
            user.PostIds = string.Join(",", postIds);
            this.DbContext.DoWithTransaction(() =>
            {
                this.DbContext.Insert(user);
                this.DbContext.Insert(logOnEntity);
                this.DbContext.InsertRange(userRoles);
                this.DbContext.InsertRange(userOrgs);
                this.DbContext.InsertRange(userPosts);
            });
        }

        public void Update(AddUserInput input)
        {
            this.Trim(input);

            SysUser user = this.DbContext.Query<SysUser>().Where(a => a.Id == input.Id).AsTracking().First();

            user.EnsureIsNotAdmin();
            if (user.State == AccountState.Closed)
                throw new InvalidInputException("无法修改已注销用户");

            string accountName = null;
            if (user.AccountName.IsNullOrEmpty())
            {
                //用户名设置后不能修改
                if (input.AccountName.IsNotNullOrEmpty())
                {
                    accountName = input.AccountName.ToLower();
                    bool exists = this.DbContext.Query<SysUser>().Where(a => a.AccountName == accountName).Any();
                    if (exists)
                        throw new InvalidInputException($"用户名[{input.AccountName}]已存在");
                }
            }
            else
                accountName = user.AccountName;

            string mobilePhone = null;
            if (user.MobilePhone.IsNotNullOrEmpty() && input.MobilePhone.IsNullOrEmpty())
            {
                //手机号码设置后不能再改为空
                throw new InvalidInputException("请输入手机号码");
            }
            if (input.MobilePhone.IsNotNullOrEmpty())
            {
                mobilePhone = input.MobilePhone;
                if (MrcUtils.IsMobilePhone(mobilePhone) == false)
                    throw new InvalidInputException("请输入正确的手机号码");

                if (user.MobilePhone != mobilePhone)//不等说明手机号码有变
                {
                    bool exists = this.DbContext.Query<SysUser>().Where(a => a.MobilePhone == mobilePhone).Any();
                    if (exists)
                        throw new InvalidInputException($"手机号码[{mobilePhone}]已存在");
                }
            }

            string email = null;
            if (user.Email.IsNotNullOrEmpty() && input.Email.IsNullOrEmpty())
            {
                //邮箱地址设置后不能再改为空
                throw new InvalidInputException("请输入邮箱地址");
            }
            if (input.Email.IsNotNullOrEmpty())
            {
                email = input.Email.ToLower();
                if (MrcUtils.IsEmail(email) == false)
                    throw new InvalidInputException("请输入正确的邮箱地址");

                if (user.Email != email)//不等说明邮箱有变
                {
                    bool exists = this.DbContext.Query<SysUser>().Where(a => a.Email == email).Any();
                    if (exists)
                        throw new InvalidInputException($"邮箱地址[{input.Email}]已存在");
                }
            }

            user.AccountName = accountName;
            user.Name = input.Name;
            user.Gender = input.Gender;
            user.MobilePhone = mobilePhone;
            user.Birthday = input.Birthday;
            user.WeChat = input.WeChat;
            user.Email = email;
            user.Description = input.Description;

            List<string> roleIds = input.RoleIds.Split(',').ToList();
            List<SysUserRole> userRoles = this.DbContext.Query<SysUserRole>().Where(a => a.UserId == input.Id).ToList();
            List<string> userRolesToDelete = userRoles.Where(a => !roleIds.Contains(a.Id)).Select(a => a.Id).ToList();
            List<SysUserRole> userRolesToAdd = roleIds.Where(a => !userRoles.Any(r => r.Id == a)).Select(a =>
            {
                return new SysUserRole()
                {
                    Id = IdHelper.CreateStringSnowflakeId(),
                    UserId = input.Id,
                    RoleId = a,
                };
            }).ToList();

            user.RoleIds = string.Join(",", roleIds);

            List<string> orgIds = input.OrgIds.Split(',').ToList();
            List<SysUserOrg> userOrgs = this.DbContext.Query<SysUserOrg>().Where(a => a.UserId == input.Id).ToList();
            List<string> userOrgsToDelete = userOrgs.Where(a => !orgIds.Contains(a.Id)).Select(a => a.Id).ToList();
            List<SysUserOrg> userOrgsToAdd = orgIds.Where(a => !userOrgs.Any(r => r.Id == a)).Select(a =>
            {
                return new SysUserOrg()
                {
                    Id = IdHelper.CreateStringSnowflakeId(),
                    UserId = input.Id,
                    OrgId = a,
                    DisablePermission = false
                };
            }).ToList();

            user.OrgIds = string.Join(",", orgIds);

            List<string> postIds = input.PostIds.Split(',').ToList();
            List<SysUserPost> userPosts = postIds.Select(a =>
            {
                return new SysUserPost()
                {
                    Id = IdHelper.CreateStringSnowflakeId(),
                    UserId = input.Id,
                    PostId = a
                };
            }).ToList();
            user.PostIds = string.Join(",", postIds);
            this.DbContext.DoWithTransaction(() =>
            {
                //更新角色权限
                this.DbContext.Delete<SysUserRole>(a => a.Id.In(userRolesToDelete));
                this.DbContext.InsertRange(userRolesToAdd);

                //更新所属组织
                this.DbContext.Delete<SysUserOrg>(a => a.Id.In(userOrgsToDelete));
                this.DbContext.InsertRange(userOrgsToAdd);
                
                //更新所属职位
                this.DbContext.Delete<SysUserPost>(a => a.UserId == input.Id);
                this.DbContext.InsertRange(userPosts);

                this.DbContext.Update<SysUser>(user);
            });
        }
        void Trim(AddUserInput input)
        {
            if (input.AccountName.IsNotNullOrEmpty())
                input.AccountName = input.AccountName.Trim();
            if (input.MobilePhone.IsNotNullOrEmpty())
                input.MobilePhone = input.MobilePhone.Trim();
            if (input.Email.IsNotNullOrEmpty())
                input.Email = input.Email.Trim();
        }

        public void UpdatePassword(UpdateUserPwdInput input)
        {
            input.id.IsNotNullOrEmpty();
            input.password.IsNotNullOrEmpty();
            input.cpassword.IsNotNullOrEmpty();
            if(input.password!=input.cpassword) throw new InvalidInputException("密码与确认密码不一致");
            var user = this.DbContext.QueryByKey<SysUser>(input.id);
            if(user==null) throw new InvalidInputException("用户不存在");
            if(user.State == AccountState.Closed)
                throw new InvalidInputException("无法修改已注销用户");
            string userSecretkey = UserHelper.GenUserSecretkey();
            string encryptedPassword = EncryptHelper.EncryptMD5Password(input.password, userSecretkey);
            this.DbContext.DoWithTransaction(() =>
            {
                this.DbContext.Update<SysUserLogOn>(a => a.UserId == user.Id, a => new SysUserLogOn() { UserSecretkey = userSecretkey, UserPassword = encryptedPassword });
            });
        }

    }
}

﻿using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface IRabbitMQMessageService : IServiceBase<RabbitMQMessage>, IMrcService
    {

    }
    public class RabbitMQMessageService : ServiceBase<RabbitMQMessage>, IRabbitMQMessageService
    {
        public RabbitMQMessageService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}


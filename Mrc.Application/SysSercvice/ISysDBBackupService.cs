﻿using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;
using DotNetEx.ExceptionEx;
using DotNetEx;

namespace Mrc.Application
{
    public interface ISysDBBackupService : IServiceBase<SysDBBackup>, IMrcService
    {
        void AddDBBack(SysDBBackup dto, AdminSession adminSession);
        void UpdateDBBack(UpdateSysDBBackupDto dto, AdminSession adminSession);
    }
    public class SysDBBackupService : ServiceBase<SysDBBackup>, ISysDBBackupService
    {
        public SysDBBackupService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
        public void UpdateDBBack(UpdateSysDBBackupDto dto, AdminSession adminSession)
        {
            if (dto.Id.IsNullOrEmpty() || dto.Name.IsNullOrEmpty() || dto.Description.IsNullOrEmpty())
                throw new InvalidInputException("信息不能为空");
            var back = DbContext.QueryByKey<SysDBBackup>(dto.Id);
            if (back == null) throw new InvalidInputException("查找数据不存在");
            back.Name = dto.Name;
            back.Description = dto.Description;
            back.LastModifyTime = DateTime.Now;
            back.LastModifyUid = adminSession.UserId;
            DbContext.Update(back);
        }
        public void AddDBBack(SysDBBackup dto, AdminSession adminSession)
        {
            dto.CreateTime = DateTime.Now;
            dto.CreateUserId = adminSession.UserId;
            dto.RestoreTimes = 0;
            dto.Description = dto.Description;
            var key = GlobalsConfig.Configuration["Key:DBBackKey"];
            dto.Path = EncryptHelper.DesEncrypt(dto.Path.Trim(), key);
            DbContext.Insert(dto);
        }
    }
}

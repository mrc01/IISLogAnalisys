﻿
using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface ISysOrgTypeService : IServiceBase<SysOrgType>, IMrcService
    {

    }
    public class SysOrgTypeService : ServiceBase<SysOrgType>, ISysOrgTypeService
    {
        public SysOrgTypeService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}

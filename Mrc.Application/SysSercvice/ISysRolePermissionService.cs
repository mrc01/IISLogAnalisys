﻿


using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface ISysRolePermissionService : IServiceBase<SysRolePermission>, IMrcService
    {

    }
    public class SysRolePermissionService : ServiceBase<SysRolePermission>, ISysRolePermissionService
    {
        public SysRolePermissionService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}

﻿

using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface ISysPostService : IServiceBase<SysPost>, IMrcService
    {

    }
    public class SysPostService : ServiceBase<SysPost>, ISysPostService
    {
        public SysPostService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}

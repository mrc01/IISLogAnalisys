﻿using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Mrc.Application
{
    public interface ISysC2CIMService : IServiceBase<SysC2CIM>, IMrcService
    {
        Task<List<SysC2CIM>> GetListAsync(string fuid, string tuid);
    }
    public class SysC2CIMService : ServiceBase<SysC2CIM>, ISysC2CIMService
    {
        public SysC2CIMService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
        public Task<List<SysC2CIM>> GetListAsync(string fuid,string tuid)
        {
            return Task.Run(() =>
            {
                using (var context = DbContextHelper.GetDBContext())
                {
                   return context.Query<SysC2CIM>().Where(x=>x.FromUid==fuid&&x.ToUid==tuid).ToList();
                }
            });           
        }
    }
}


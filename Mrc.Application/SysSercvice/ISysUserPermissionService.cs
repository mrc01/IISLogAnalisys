﻿


using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface ISysUserPermissionService : IServiceBase<SysUserPermission>, IMrcService
    {

    }
    public class SysUserPermissionService : ServiceBase<SysUserPermission>, ISysUserPermissionService
    {
        public SysUserPermissionService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}

﻿

using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface ISysUserRoleService : IServiceBase<SysUserRole>, IMrcService
    {

    }
    public class SysUserRoleService : ServiceBase<SysUserRole>, ISysUserRoleService
    {
        public SysUserRoleService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}

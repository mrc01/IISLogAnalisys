﻿
using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface ISysOrgPermissionService : IServiceBase<SysOrgPermission>, IMrcService
    {

    }
    public class SysOrgPermissionService : ServiceBase<SysOrgPermission>, ISysOrgPermissionService
    {
        public SysOrgPermissionService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}

﻿using Chloe;
using Microsoft.AspNetCore.Http;
using Mrc.Data;
using Mrc.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Application
{
    public static class ServiceHelper
    {
        public static T GetService<T>(HttpContext httpContext)where T:class
        {
            return httpContext.RequestServices.GetService(typeof(T)) as T;
        }
    }

    public class ServiceEX
    {
        private static IDbContext dbContext;
        public static IDbContext GetDbContext()
        {
            if(dbContext==null) dbContext = DbContextHelper.GetDBContext();
            return dbContext;
        }
        public ServiceEX() => dbContext = DbContextHelper.GetDBContext();
        public ServiceEX(IDbContext Context) => dbContext = Context;
      
        public static ServiceBase<TEntity> GetServiceBase<TEntity>()where TEntity: BaseEntity
        {
            return new ServiceBase<TEntity>(GetDbContext());
        }
    }
}

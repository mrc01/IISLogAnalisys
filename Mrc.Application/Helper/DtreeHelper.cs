﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：DotNetEx.Helpers 
* 项目描述 ： 
* 类 名 称 ：DtreeHelper 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：DotNetEx.Helpers
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/2/24 23:53:03 
* 更新时间 ：2019/2/24 23:53:03 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using Mrc.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mrc.Application
{
    public class DtreeHelper<T> where T : ParentBaseEntity
    {
        private List<T> DataList;
        private List<string> myKey;
        private Dictionary<int, string> typeName;
        public DtreeHelper(List<T> dataList, Dictionary<int, string> typeName = null, List<string> myKey = null)
        {
            this.DataList = dataList;
            this.myKey = myKey == null ? new List<string>() : myKey;
            this.typeName= typeName == null ? new Dictionary<int, string>(): typeName;
        }
        ///递归获取所有树结构的数据
        public List<DTree> GetData()
        {
            List<DTree> nodes = DataList.Where(x => x.ParentId == null).Select(x => new DTree
            {
                id = x.Id,
                parentId = x.ParentId,
                title = $"{x.Name} ({(typeName.Any()? typeName[x.Type??0]:"")})",
                level = "1",
                basicData = x
            }).ToList();
            foreach (var item in nodes)
            {
                item.children = GetChildrens(item, 2);
                var checkArr = new List<CheckArr>();
                checkArr.Add(new CheckArr("0", myKey.Contains(item.id) ? "1" : "0"));
                item.checkArr = checkArr;
                item.isLast = DataList.Where(x => x.ParentId == item.id).Any() ? false : true;
            }
            DataList = null;
            return nodes;
        }
        //递归获取子节点
        public List<DTree> GetChildrens(DTree node, int level)
        {
            List<DTree> childrens = DataList.Where(x => x.ParentId == node.id).Select(x => new DTree
            {
                id = x.Id,
                parentId = x.ParentId,
                title = $"{x.Name} ({(typeName.Any() ? typeName[x.Type ?? 0] : "")})",
                level = level.ToString(),
                basicData = x
            }).ToList();
            foreach (DTree item in childrens)
            {
                item.children = GetChildrens(item, (level + 1));
                item.isLast = DataList.Where(x => x.ParentId == item.id).Any() ? false : true;
                var checkArr = new List<CheckArr>();
                checkArr.Add(new CheckArr("0", myKey.Contains(item.id) ? "1" : "0"));
                item.checkArr = checkArr;
            }
            return childrens;
        }
    }


    public class NavDtreeHelper
    {
        private List<SysPermission> DataList;
        private Dictionary<string, string> myKey;
        private AdminSession adminSession;
        public NavDtreeHelper(AdminSession adminSession,List<SysPermission> dataList,Dictionary<string,string> myKey = null)
        {
            this.DataList = dataList;
            this.myKey = myKey == null ? new Dictionary<string, string>(): myKey;
            this.adminSession = adminSession;
        }
        public Task<List<DTree>> GetData()
        {
            List<DTree> ret = new List<DTree>();
            List<SysPermission> parentPermissions = DataList.Where(a => a.ParentId == null).ToList();
            foreach (SysPermission item in parentPermissions)
            {
                var parentNode = new DTree
                {
                    id = item.Id,
                    parentId = item.ParentId,
                    title = $"{item.Name}",
                    basicData = item,
                    sortCode=item.SortCode??0
                };
                List<DTree> childMenus = new List<DTree>();
                GatherChildMenus(item, childMenus);
                parentNode.children.AddRange(childMenus.OrderBy(x=>x.sortCode).ToList());
                ret.Add(parentNode);
            }
            ret = ret.Where(a => !((a.basicData as SysPermission).Type == (int)PermissionType.节点组 && a.children.Count == 0)).OrderBy(x=>x.sortCode).ToList();
            return  Task.Run(() => ret);
        }
        void GatherChildMenus(SysPermission permission, List<DTree> list)
        {
            var childPermissions = DataList.Where(a => a.ParentId == permission.Id).OrderBy(a => a.SortCode);
            foreach (SysPermission childPermission in childPermissions)
            {
                if (childPermission.Type ==(int)PermissionType.节点组)
                {
                    GatherChildMenus(childPermission, list);
                    continue;
                }
                if (childPermission.Type != (int)PermissionType.公共菜单 && !adminSession.IsAdmin)
                {
                    if (!myKey.ContainsKey(childPermission.Id))
                        continue;
                }
                list.Add(new DTree
                {
                    id = childPermission.Id,
                    parentId = childPermission.ParentId,
                    title = $"{childPermission.Name}",
                    level = "1",
                    basicData = childPermission,
                    sortCode = childPermission.SortCode ?? 0
                });
            }
        }
    }
}

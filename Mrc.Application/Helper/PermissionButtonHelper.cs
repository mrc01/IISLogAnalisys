﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：DotNetEx.Helpers 
* 项目描述 ： 
* 类 名 称 ：PermissionButtonHelper 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：DotNetEx.Helpers
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/2/20 18:38:25 
* 更新时间 ：2019/2/20 18:38:25 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using Microsoft.AspNetCore.Http;
using Mrc.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Application
{
    public static class PermissionButtonHelper
    {
        public static string CreateButtonHtml(List<string> permissions, HttpContext httpContext)
        {
            var usePermits = httpContext.Items["User_Permits"] as List<string>;
            string html = "";
            AdminSession session = httpContext.Items["user"] as AdminSession;
            if (session.IsAdmin == true)
            {
                usePermits = permissions;
                //usePermits.Add("system.user.revise_password");
            }
            foreach (var item in permissions)
            {
                if (usePermits.Contains(item))
                {
                    if (item.Contains("add"))
                    {
                        html += "<button class=\"layui-btn layui-btn-sm\" lay-event=\"add\">添加</button>";
                    }
                    else if (item.Contains("delete"))
                    {
                        html += "<button class=\"layui-btn layui-btn-sm\" lay-event=\"delete\">删除</button>";
                    }
                    else if (item.Contains("update"))
                    {
                        html += "<button class=\"layui-btn layui-btn-sm\" lay-event=\"update\">修改</button>";
                    }
                    else if (item.Contains("revise_password"))
                    {
                        html += "<button class=\"layui-btn layui-btn-sm\" lay-event=\"updatepassword\">更改密码</button>";
                    }
                }
            }
            return html;
        }
    }
}

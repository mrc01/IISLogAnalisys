﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Application.ProjectService 
* 项目描述 ： 
* 类 名 称 ：IUserProjectTaskService 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Application.ProjectService
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/3/6 22:43:32 
* 更新时间 ：2019/3/6 22:43:32 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;
using DotNetEx.ExceptionEx;
using System.Linq;

namespace Mrc.Application
{
    public interface IUserProjectTaskService : IServiceBase<UserProjectTask>, IMrcService
    {
        void AddTask(AddProjectTaskDto userProjectTaskDto, AdminSession adminSession);
        void UpdateTask(UpdateProjectTaskDto updateProjectTaskDto, AdminSession adminSession);
    }
    public class UserProjectTaskService : ServiceBase<UserProjectTask>, IUserProjectTaskService
    {
        public UserProjectTaskService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
        public void AddTask(AddProjectTaskDto dto, AdminSession adminSession)
        {
            if(dto.Name==null) throw new InvalidInputException("请填写任务名称");
            if (dto.ExecuteUId == null) throw new InvalidInputException("请添加任务执行人");
            if (dto.PlanStarTime == null) throw new InvalidInputException("请选择任务开始时间");
            if (dto.PlanFinishTime == null) throw new InvalidInputException("请选择任务完成时间");
            if (dto.Priority == null) throw new InvalidInputException("请选择任务紧急等级");
            if (dto.Type == null) throw new InvalidInputException("请选择任务类型");
            // if (dto.userIds == null) throw new InvalidInputException("请选择关联人员");
            var demand = DbContext.QueryByKey<UserProjectDemand>(dto.DemandId ?? "");
            var executeUser= DbContext.QueryByKey<SysUser>(dto.ExecuteUId ?? "");
            if(executeUser==null) throw new InvalidInputException("请添加正确的任务执行人");
            UserProjectTask task = new UserProjectTask();
            if (demand == null)
            {
                task.DemandName = "";
                if (dto.ProjectId == null) throw new InvalidInputException("请选择关联项目");
                var project = DbContext.QueryByKey<UserProject>(dto.ProjectId);
                if (project == null) throw new InvalidInputException("项目不存在");
                task.ProjectId = project.Id;
                task.ProjectName = project.Name;
            }
            else
            {
                task.DemandName = demand.Name;
                task.ProjectId = demand.ProjectId;
                task.ProjectName = demand.ProjectName;
            }
            task.ExecuteUId = executeUser.Id;
            task.ExecuteUser = executeUser.Name;
            task.CreateTime = DateTime.Now;
            task.Type = dto.Type;
            task.PlanStarTime = dto.PlanStarTime;
            task.PlanFinishTime = dto.PlanFinishTime;
            task.Priority = dto.Priority;
            task.Description = dto.Description;
            task.CreateUserId = adminSession.UserId;
            task.Process = 0;
            task.Type = 0;
            task.Status = 0;
            task.Name = dto.Name;
            //添加关联人员
            List<UserProjectMember> userProjectMembers = new List<UserProjectMember>();
            if (!dto.userIds.IsNullOrEmpty())
            {
                var Ids = dto.userIds.Split(',').ToList();
                var allUser = this.DbContext.Query<SysUser>().Where(x => Ids.Contains(x.Id)||x.Id== dto.ExecuteUId).ToList();
                foreach (var user in allUser)
                {
                    UserProjectMember member = new UserProjectMember();
                    member.CreateTime = DateTime.Now;
                    member.CreateUserId = adminSession.UserId;
                    member.IsDeleted = false;
                    member.IsEnabled = true;
                    member.ProjectId = task.ProjectId;
                    member.ProjectName = task.ProjectName;
                    member.UserID = user.Id;
                    member.UserName = user.Name;
                    member.ProjectDemandId = task.DemandId;
                    member.TaskId = task.Id;
                    userProjectMembers.Add(member);
                }
            }
            DbContext.DoWithTransaction(() =>
            {
                DbContext.Insert(task);
                if (userProjectMembers.Count > 0) this.DbContext.InsertRange(userProjectMembers);
            });
        }
        public void UpdateTask(UpdateProjectTaskDto dto, AdminSession adminSession)
        {
            if (dto.Id == null) throw new InvalidInputException("查询该任务失败");
            if (dto.Name == null) throw new InvalidInputException("请填写任务名称");
            if (dto.ExecuteUId == null) throw new InvalidInputException("请添加任务执行人");
            if (dto.PlanStarTime == null) throw new InvalidInputException("请选择任务开始时间");
            if (dto.PlanFinishTime == null) throw new InvalidInputException("请选择任务完成时间");
            if (dto.Priority == null) throw new InvalidInputException("请选择任务紧急等级");
            if (dto.Type == null) throw new InvalidInputException("请选择任务类型");
            if (dto.Status == null) throw new InvalidInputException("请选择当前任务状态");
            var demand = DbContext.QueryByKey<UserProjectDemand>(dto.DemandId??"");
            var task = DbContext.QueryByKey<UserProjectTask>(dto.Id);
            var executeUser = DbContext.QueryByKey<SysUser>(dto.ExecuteUId ?? "");
            if (executeUser == null) throw new InvalidInputException("请添加正确的任务执行人");
            // if(demand==null||task==null) throw new InvalidInputException("查找数据失败");
            if (demand == null)
            {
                task.DemandName = "";
                if (dto.ProjectId == null) throw new InvalidInputException("请选择关联项目");
                var project = DbContext.QueryByKey<UserProject>(dto.ProjectId);
                if (project == null) throw new InvalidInputException("项目不存在");
                task.ProjectId = project.Id;
                task.ProjectName = project.Name;
            }
            else if (demand.Id != task.DemandId)
            {
                task.DemandName = demand.Name;
                task.ProjectId = demand.ProjectId;
                task.ProjectName = demand.ProjectName;
            }

            task.ExecuteUId = executeUser.Id;
            task.ExecuteUser = executeUser.Name;
            task.PlanStarTime = dto.PlanStarTime;
            task.PlanFinishTime = dto.PlanFinishTime;
            task.Priority = dto.Priority;
            task.Description = dto.Description;
            task.CreateUserId = adminSession.UserId;
            task.Process = dto.Process;
            task.Type = dto.Type;
            task.Status = dto.Status;
            task.Name = dto.Name;
            task.Status = dto.Status;
            //添加关联人员
            List<UserProjectMember> userProjectMembers = new List<UserProjectMember>();
            if (!dto.userIds.IsNullOrEmpty())
            {
                var Ids = dto.userIds.Split(',').ToList();
                var allUser = this.DbContext.Query<SysUser>().Where(x => Ids.Contains(x.Id)).ToList();
                foreach (var user in allUser)
                {
                    UserProjectMember member = new UserProjectMember();
                    member.CreateTime = DateTime.Now;
                    member.CreateUserId = adminSession.UserId;
                    member.IsDeleted = false;
                    member.IsEnabled = true;
                    member.ProjectId = task.ProjectId;
                    member.ProjectName = task.ProjectName;
                    member.UserID = user.Id;
                    member.UserName = user.Name;
                    member.ProjectDemandId = task.DemandId;
                    member.TaskId = task.Id;
                    userProjectMembers.Add(member);
                }
            }
            DbContext.DoWithTransaction(() =>
            {
                DbContext.Update(task);
                this.DbContext.Delete<UserProjectMember>(x => x.TaskId == task.Id);
                if (userProjectMembers.Count > 0) this.DbContext.InsertRange(userProjectMembers);
            });
        }
    }
}
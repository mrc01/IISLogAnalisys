﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Application.ProjectService 
* 项目描述 ： 
* 类 名 称 ：IUserProjectService 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Application.ProjectService
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/3/4 22:58:54 
* 更新时间 ：2019/3/4 22:58:54 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;

namespace Mrc.Application
{
    public interface IUserProjectService : IServiceBase<UserProject>, IMrcService
    {

    }
    public class UserProjectService : ServiceBase<UserProject>, IUserProjectService
    {
        public UserProjectService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
    }
}

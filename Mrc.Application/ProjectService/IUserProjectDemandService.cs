﻿using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;
using System.Linq;
using DotNetEx;
using DotNetEx.ExceptionEx;

namespace Mrc.Application
{
    public interface IUserProjectDemandService : IServiceBase<UserProjectDemand>, IMrcService
    {
        void AddDemand(AddUpdateUserProjectDemand input, string createUid);
        void UpdateDemand(AddUpdateUserProjectDemand input, string createUid);
    }
    public class UserProjectDemandService : ServiceBase<UserProjectDemand>, IUserProjectDemandService
    {
        public UserProjectDemandService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }
        public void AddDemand(AddUpdateUserProjectDemand input, string createUid)
        {
            List<UserProjectMember> userProjectMembers = new List<UserProjectMember>();
            input.Status = (int)ProjectStatusEnum.待下发;
            if (!input.userIds.IsNullOrEmpty())
            {
                List<string> Ids = input.userIds.Split(',').ToList();
                var allUser = this.DbContext.Query<SysUser>().Where(x => Ids.Contains(x.Id)).ToList();
                foreach (var user in allUser)
                {
                    UserProjectMember member = new UserProjectMember();
                    member.CreateTime = DateTime.Now;
                    member.CreateUserId = createUid;
                    member.IsDeleted = false;
                    member.IsEnabled = true;
                    member.ProjectId = input.ProjectId;
                    member.ProjectName = input.ProjectName;
                    member.UserID = user.Id;
                    member.UserName = user.Name;
                    member.ProjectDemandId = input.Id;
                    userProjectMembers.Add(member);
                }
            }
            UserProjectDemand projectDemand = new UserProjectDemand();
            MapEx.CopyModel(projectDemand, input);
            projectDemand.CreateTime = DateTime.Now;
            projectDemand.CreateUserId = createUid;
            projectDemand.Process = 0;
            this.DbContext.DoWithTransaction(() =>
            {
                this.DbContext.Insert(projectDemand);
                if (userProjectMembers.Count > 0) this.DbContext.InsertRange(userProjectMembers);
            });
        }

        public void UpdateDemand(AddUpdateUserProjectDemand input, string createUid)
        {
            var demand = this.DbContext.QueryByKey<UserProjectDemand>(input.Id);
            if (demand == null) throw new InvalidInputException("需求不存在");
            if (input.Name.IsNullOrEmpty()) throw new InvalidInputException("需求名称不能为空");
            if (input.Status < 0) throw new InvalidInputException("请选择当前需求状态");
            if (input.Type < 0) throw new InvalidInputException("请选择需求类型");
            List<UserProjectMember> userProjectMembers = new List<UserProjectMember>();
            if (!input.userIds.IsNullOrEmpty())
            {
                List<string> Ids = input.userIds.Split(',').ToList();
                var allUser = this.DbContext.Query<SysUser>().Where(x => Ids.Contains(x.Id)).ToList();
                foreach (var user in allUser)
                {
                    UserProjectMember member = new UserProjectMember();
                    member.CreateTime = DateTime.Now;
                    member.CreateUserId = createUid;
                    member.IsDeleted = false;
                    member.IsEnabled = true;
                    member.ProjectId = input.ProjectId;
                    member.ProjectName = input.ProjectName;
                    member.UserID = user.Id;
                    member.UserName = user.Name;
                    member.ProjectDemandId = input.Id;
                    userProjectMembers.Add(member);
                }
            }
            demand.Priority = input.Priority;
            demand.Name = input.Name;
            demand.Status = input.Status;
            demand.Type = input.Type;
            demand.Endtime = input.Endtime;
            demand.Process = input.Process;
            demand.Description = input.Description;
            this.DbContext.DoWithTransaction(() =>
            {
                this.DbContext.Update(demand);
                this.DbContext.Delete<UserProjectMember>(x => x.ProjectDemandId == demand.Id);
                if (userProjectMembers.Count > 0) this.DbContext.InsertRange(userProjectMembers);
            });
        }

    }
}

﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Application.ProjectService 
* 项目描述 ： 
* 类 名 称 ：IUserProjectTaskService 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Application.ProjectService
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/3/6 22:43:32 
* 更新时间 ：2019/3/6 22:43:32 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using Mrc.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Mrc.Entity;
using Chloe;
using System.Linq.Expressions;
using DotNetEx.ExceptionEx;
using System.Linq;

namespace Mrc.Application
{
    public interface IUserProjectBugService : IServiceBase<UserProjectBug>, IMrcService
    {
        void AddBug(AddProjectBugDto dto, AdminSession adminSession);
        void UpdateBug(UpdateProjectBugDto dto, AdminSession adminSession);
        void UpdateStatus(string id, int status);

        void DeleteBugs(string[] ids, AdminSession adminSession);
    }
    public class UserProjectBugService : ServiceBase<UserProjectBug>, IUserProjectBugService
    {
        public UserProjectBugService(IDbContext dbContext, IServiceProvider services) : base(dbContext, services)
        {

        }

        /// <summary>
        /// 添加bug
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="adminSession"></param>
        public void AddBug(AddProjectBugDto dto, AdminSession adminSession)
        {
            if(dto.Name==null) throw new InvalidInputException("请填写bug标题");
            if(dto.ExecuteUId == null) throw new InvalidInputException("请添加任务执行人");
            var task= DbContext.QueryByKey<UserProjectTask>(dto.TaskId??"");
            if(task==null) throw new InvalidInputException("请选择关联任务");
            var demand = DbContext.QueryByKey<UserProjectDemand>(task.DemandId ?? "");
            var executeUser= DbContext.QueryByKey<SysUser>(dto.ExecuteUId ?? "");
            if(executeUser==null) throw new InvalidInputException("请添加正确的执行人");
            UserProjectBug bug = new UserProjectBug();
            bug.ExecuteUId = executeUser.Id;
            bug.ExecuteUser = executeUser.Name;
            if(demand == null)
            {
                bug.DemandName = "";
                if (task.ProjectId == null) throw new InvalidInputException("当前任务无法找到关联项目");
                var project = DbContext.QueryByKey<UserProject>(task.ProjectId);
                if (project == null) throw new InvalidInputException("项目不存在");
                bug.ProjectId = project.Id;
                bug.ProjectName = project.Name;
            }
            else
            {
                bug.DemandName = demand.Name;
                bug.DemandId = demand.Id;
                bug.ProjectId = demand.ProjectId;
                bug.ProjectName = demand.ProjectName;
            }
            bug.TaskId = task.Id;
            bug.TaskName = task.Name;
            bug.CreateTime = DateTime.Now;
            bug.Priority = 0;
            bug.Description = dto.Description;
            bug.CreateUserId = adminSession.UserId;
            bug.Type = 0;
            bug.Status = 0;
            bug.SeverityLevel = dto.SeverityLevel ?? 0;
            bug.Name = dto.Name;
            DbContext.Insert(bug);
        }
        /// <summary>
        /// 更新bug
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="adminSession"></param>
        public void UpdateBug(UpdateProjectBugDto dto, AdminSession adminSession)
        {
            if (dto.Id == null) throw new InvalidInputException("查询该bug失败");
            if (dto.Name == null) throw new InvalidInputException("请填写bug名称");
            if (dto.ExecuteUId == null) throw new InvalidInputException("请添加bug执行人");
            if (dto.Status == null) throw new InvalidInputException("请选择当前bug状态");
            var bug=DbContext.QueryByKey<UserProjectBug>(dto.Id??"");
            var task = DbContext.QueryByKey<UserProjectTask>(dto.TaskId);
            var executeUser = DbContext.QueryByKey<SysUser>(dto.ExecuteUId ?? "");
            if (executeUser == null) throw new InvalidInputException("请添加正确的bug执行人");
            var demand = DbContext.QueryByKey<UserProjectDemand>(task.DemandId ?? "");
            if (demand == null)
            {
                bug.DemandName = "";
                if (task.ProjectId == null) throw new InvalidInputException("请选择关联项目");
                var project = DbContext.QueryByKey<UserProject>(task.ProjectId);
                if (project == null) throw new InvalidInputException("项目不存在");
                bug.ProjectId = project.Id;
                bug.ProjectName = project.Name;
            }
            else if (demand.Id != task.DemandId)
            {
                bug.DemandName = demand.Name;
                bug.ProjectId = demand.ProjectId;
                bug.ProjectName = demand.ProjectName;
            }
            bug.TaskId = task.Id;
            bug.TaskName = task.Name;
            bug.ExecuteUId = executeUser.Id;
            bug.ExecuteUser = executeUser.Name;
            bug.Priority = dto.Priority??0;
            bug.Description = dto.Description;
            bug.Status = dto.Status;
            bug.Name = dto.Name;
           // bug.Remark = dto.Remark;
            DbContext.Update(bug);
        }
        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        public void UpdateStatus(string id,int status)
        {
            if (id == null) throw new InvalidInputException("请选择更新的bug");
            var bug = DbContext.QueryByKey<UserProjectBug>(id);
            if (bug == null) throw new InvalidInputException("bug信息不存在");
            var statusDic =EnumExtension.GetDic<ProjectBugStatusEnum>();
            if(!statusDic.ContainsKey(status)) throw new InvalidInputException("不包含当前状态");
            bug.Status = status;
            DbContext.Update(bug);
        }
        /// <summary>
        /// 删除bug
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="adminSession"></param>
        public void DeleteBugs(string[] ids,AdminSession adminSession)
        {
            foreach (var item in ids)
            {
                this.SoftDelete(item, adminSession.UserId);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
namespace RabbitMQServer.Huobi.Models
{
    public class Prices
    {
        public string name { get; set; }
        public decimal rise_percent { get; set; }
        public decimal close { get; set; }
        public decimal open { get; set; }
    }
}

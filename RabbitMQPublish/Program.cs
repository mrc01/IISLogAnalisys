﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotNetEx;
using RabbitMQServer.Huobi.Models;
using Mrc.RabbitMq.Helper;
using EasyNetQ;
using Mrc.Entity.RabbitMqEntity;

namespace RabbitMQPublish
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var bus = RabbitHutch.CreateBus("host=42.51.42.120;port=5672;virtualHost=EasyNetQ; username=xman;password=xman01111"))
            {
                while (true)
                {
                    bus.PublishAsync(new EasyNetQTestMessage { Msg = "hello world" });                    
                    Thread.Sleep(1);
                }              
            }
        }
        private static Dictionary<string, string> GetNames()
        {
            Dictionary<string, string> names = new Dictionary<string, string>();
            names.Add("btc", "btc");
            names.Add("eos", "eos");
            names.Add("eth", "eth");
            names.Add("ltc", "ltc");
            names.Add("bch", "bch");
            names.Add("ht", "ht");
            names.Add("trx", "trx");
            names.Add("xrp", "xrp");
            names.Add("ont", "ont");
            names.Add("etc", "etc");
            return names;
        }
    }

    public class message
    {
        public string msg { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Mrc.Data;
using Microsoft.AspNetCore.Http.Features;
using Mrc.Application.Middleware;
using Mrc.Application.Filter;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace MrcRabbitMQWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            var builder = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath)
          .AddJsonFile(Path.Combine("configs", "appsettings.json"), optional: true, reloadOnChange: true)
          .AddEnvironmentVariables();
            Configuration = builder.Build();
            GlobalsConfig.SetBaseConfig(Configuration, env.ContentRootPath, env.WebRootPath);
           // Task.Factory.StartNew(() => Mrc.RabbitMq.ConsumeTest.StartRun());
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(this.Configuration);
            services.AddDatabase();/*数据上下文*/
            services.RegisterAppServices(); /* 注册应用服务 */
            services.AddMemoryCache();/* 缓存 */
            services.AddSession();/*  Session  */


            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            }).AddCookie(options =>
            {
            });

            services.AddResponseCaching();
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                options.Filters.Add(typeof(Mrc.Application.WebPermissionFilter));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = 2147483647;
                x.MultipartBodyLengthLimit = 2147483647; //2G
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            GlobalsConfig.Services = app.ApplicationServices;

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseResponseCaching();
            app.UseFilterIP();
            app.UseWriteLoginInfo();//登陆中间件
            app.UseMvc(routes =>
            {
                routes.MapRoute("areaRoute", "{area:exists}/{controller}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

﻿/*******************************************************************************
 * Author: mrc
 * Description:LayuiTable 数据表格接收的格式model 使用例子: _accessoryService.List().ToLayuiTable(1, 10); 
 * datetime：17-9-28
*********************************************************************************/
using Chloe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System
{
    public class LayuiTableJson<T>
    {
        /// <summary>
        /// 返回状态码
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 信息
        /// </summary>
        public string msg { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int count { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<T> data { get; set; }

        public LayuiTableJson()
        {
            data = new List<T>();
        }
    }

    public static class PageHelperExt2
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static LayuiTableJson<T> ToLayuiTable<T>(this IEnumerable<T> source, int page, int pageSize,string msg="")
        {
            LayuiTableJson<T> result = new LayuiTableJson<T>();
            result.count = source.Count();
            result.data = source.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            result.code = 0;
            result.msg = "OK";
            return result;
        }
        public static LayuiTableJson<T> ToLayuiTable<T>(this IQuery<T> source, int page, int pageSize, string msg = "")
        {
            LayuiTableJson<T> result = new LayuiTableJson<T>();
            result.count = source.Count();
            result.data = source.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            result.code = 0;
            result.msg = "OK";
            return result;
        }
    }
}
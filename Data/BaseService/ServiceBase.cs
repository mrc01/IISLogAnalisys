﻿using Chloe;
using Mrc.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mrc.Data
{
    public class ServiceBase<TEntity> :IDisposable, IServiceBase<TEntity> where TEntity: BaseEntity
    {
        protected ServiceBase(IDbContext dbContext, IServiceProvider services)
        {
            this.DbContext = dbContext;
            this.Services = services;
        }
        public ServiceBase(IDbContext dbContext)
        {
            this.DbContext = dbContext;
        }
        public IDbContext DbContext { get; set; }
        public IServiceProvider Services { get; set; }

        /// <summary>
        /// 插入单条数据
        /// </summary>
        /// <param name="TEntity"></param>
        public virtual void Add(TEntity TEntity)
        {
            this.DbContext.DoWithTransaction(() =>
            {
                this.DbContext.Insert(TEntity);
            });
        }
        /// <summary>
        /// 批量插入
        /// </summary>
        /// <param name="ListEntity"></param>
        public virtual void Add(List<TEntity> ListEntity)
        {
            this.DbContext.DoWithTransaction(() =>
            {
                this.DbContext.InsertRange(ListEntity);
            });
        }
        /// <summary>
        /// 删除单条数据，通过主键
        /// </summary>
        /// <param name="id"></param>
        public virtual void Delete(object id)
        {
            this.DbContext.DeleteByKey<TEntity>(id);
        }
        /// <summary>
        /// 根据条件删除
        /// </summary>
        /// <param name="where"></param>
        public virtual void Delete(Expression<Func<TEntity, bool>> where)
        {
            this.DbContext.Delete(where);
        }
        /// <summary>
        /// 软删除
        /// </summary>
        /// <param name="id"></param>
        /// <param name="operatorId"></param>
        public virtual void SoftDelete(object id,string operatorId)
        {
            this.DbContext.SoftDelete<TEntity>(id, operatorId);
        }
        /// <summary>
        /// IQuery All List 
        /// </summary>
        /// <returns></returns>
        public virtual IQuery<TEntity> List()
        {
            return this.DbContext.Query<TEntity>().Where(x => true);
        }
        public virtual IQuery<TEntity> Query()
        {
            return this.DbContext.Query<TEntity>();
        }



        /// <summary>
        /// 根据条件查询
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual IQuery<TEntity> List(Expression<Func<TEntity, bool>> where)
        {
            return this.DbContext.Query<TEntity>().Where(where).Where(x=>x.IsDeleted!=true||x.IsDeleted==null);
        }
        /// <summary>  
        /// 更新单条数据
        /// </summary>
        /// <param name="Entity"></param>
        public virtual void Update(TEntity Entity)
        {
            this.DbContext.Update(Entity);
        }
        /// <summary>
        /// Update<User>(a => a.Id == 1, a => new User() { Name = "lu", Age = a.Age+ 1, Gender = Gender.Woman, OpTime = DateTime.Now })
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="content"></param>
        /// <param name="table"></param>
        public virtual void Update(Expression<Func<TEntity, bool>> condition, Expression<Func<TEntity, TEntity>> content, string table)
        {
            this.DbContext.Update(condition,content,table);
        }
        /// <summary>
        /// 获取单条数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TEntity Single(object id)
        {
            return this.DbContext.QueryByKey<TEntity>(id);
        }
        /// <summary>
        /// 根据条件获取单条数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual TEntity Single(Expression<Func<TEntity, bool>> where)
        {
            return this.DbContext.Query<TEntity>().Where(where).First();
        }
        public void Dispose()
        {
            if (this.DbContext != null)
            {
                this.DbContext.Dispose();
            }
            this.Dispose(true);
        }     
        protected virtual void Dispose(bool disposing)
        {

        }

    }
}

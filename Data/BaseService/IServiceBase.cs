﻿using Chloe;
using Mrc.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mrc.Data
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        void Add(TEntity entity);
        void Add(List<TEntity> entity);
        void Delete(object id);
        void Delete(Expression<Func<TEntity, bool>> where);
        void SoftDelete(object Id, string operatorId);
        IQuery<TEntity> List();
        IQuery<TEntity> List(Expression<Func<TEntity, bool>> where);
        void Update(TEntity entity);
        void Update(Expression<Func<TEntity, bool>> condition, Expression<Func<TEntity, TEntity>> content, string table);
        TEntity Single(object id);
        TEntity Single(Expression<Func<TEntity, bool>> where);
        IQuery<TEntity> Query();
    }
}

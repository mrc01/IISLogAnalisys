﻿using Chloe;
using Mrc.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Mrc.Data
{
    public static  class ServiceExtension
    {

        /// <summary>
        /// 过滤
        /// </summary>
        /// <param name="source"></param>
        /// <param name="isAdmin"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public static IQuery<TEntity> AdminFilter<TEntity>(this IQuery<TEntity> source, AdminSession adminSession, Expression<Func<TEntity, bool>> where = null)
        {
            if (adminSession.IsAdmin != true) return source.Where(where);
            return source;
        }




    }
}

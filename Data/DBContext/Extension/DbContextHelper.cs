﻿
using System;
using System.Collections.Generic;
using System.Text;
using Chloe;
using Chloe.SqlServer;

namespace Mrc.Data
{
    public class DbContextHelper
    {
        public static IDbContext GetDBContext()
        {
            IDbContext dbContext;
            string connString = GlobalsConfig.Configuration["db:ConnString"];
            string dbType = GlobalsConfig.Configuration["db:DbType"];
            switch (dbType)
            {
                case "sqlserver":
                    dbContext = CreateSqlServerContext(connString);
                    break;
                default:
                    dbContext = CreateSqlServerContext(connString);
                    break;
            }
            return dbContext;
        }
        static IDbContext CreateSqlServerContext(string connString)
        {
            MsSqlContext dbContext = new MsSqlContext(connString);
            dbContext.PagingMode = PagingMode.OFFSET_FETCH;
            return dbContext;
        }
    }
}

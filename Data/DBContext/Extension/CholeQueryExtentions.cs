﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：Mrc.Data.DBContext.Extension 
* 项目描述 ： 
* 类 名 称 ：CholeQueryExtentions 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：Mrc.Data.DBContext.Extension
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/2/25 21:58:58 
* 更新时间 ：2019/2/25 21:58:58 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using Chloe;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Mrc.Data
{
    public static class ChloeQueryExtension
    {
        /// <summary>
        /// q.Where(a => a.IsDeleted == false)
        /// </summary>
        /// <typeparam name="TEntity">必须含有 IsDeleted 属性</typeparam>
        /// <param name="q"></param>
        /// <returns></returns>
        public static IQuery<TEntity> FilterDeleted<TEntity>(this IQuery<TEntity> q)
        {
            Type entityType = typeof(TEntity);

            PropertyInfo prop_IsDeleted = entityType.GetProperty("IsDeleted");

            if (prop_IsDeleted == null)
                throw new ArgumentException("实体类型未定义 IsDeleted 属性");

            ParameterExpression parameter = Expression.Parameter(entityType, "a");
            Expression prop = Expression.Property(parameter, prop_IsDeleted);

            Expression falseValue = Expression.Constant(false, prop_IsDeleted.PropertyType);

            Expression lambdaBody = Expression.Equal(prop, falseValue);

            Expression<Func<TEntity, bool>> predicate = Expression.Lambda<Func<TEntity, bool>>(lambdaBody, parameter);

            return q.Where(predicate);
        }
        /// <summary>
        /// q.Where(a => a.IsEnabled == true)
        /// </summary>
        /// <typeparam name="TEntity">必须含有 IsEnabled 属性</typeparam>
        /// <param name="q"></param>
        /// <returns></returns>
        public static IQuery<TEntity> FilterDisabled<TEntity>(this IQuery<TEntity> q)
        {
            Type entityType = typeof(TEntity);

            PropertyInfo prop_IsEnabled = entityType.GetProperty("IsEnabled");

            if (prop_IsEnabled == null)
                throw new ArgumentException("实体类型未定义 IsEnabled 属性");

            ParameterExpression parameter = Expression.Parameter(entityType, "a");
            Expression prop = Expression.Property(parameter, prop_IsEnabled);

            Expression trueValue = Expression.Constant(true, prop_IsEnabled.PropertyType);

            Expression lambdaBody = Expression.Equal(prop, trueValue);

            Expression<Func<TEntity, bool>> predicate = Expression.Lambda<Func<TEntity, bool>>(lambdaBody, parameter);

            return q.Where(predicate);
        }
        /// <summary>
        /// q.Where(a => a.IsDeleted == false &amp;&amp; a.IsEnabled == true)
        /// </summary>
        /// <typeparam name="T">必须含有 IsDeleted 和 IsEnabled 属性</typeparam>
        /// <param name="q"></param>
        /// <returns></returns>
        public static IQuery<TEntity> FilterDeletedAndDisabled<TEntity>(this IQuery<TEntity> q)
        {
            return q.FilterDeleted().FilterDisabled();
        }
    }

}

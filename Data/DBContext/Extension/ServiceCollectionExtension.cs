﻿using Chloe;
using Microsoft.Extensions.Configuration;
using Mrc.Data;
using Mrc.Data.DBContext;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddDatabase(this IServiceCollection services)
        {
            string connString = GlobalsConfig.Configuration["db:ConnString"];
            string dbType = GlobalsConfig.Configuration["db:DbType"];

            IDbContextFactory dbContextFactory = new DefaultDbContextFactory(dbType, connString);
            services.AddSingleton<IDbContextFactory>(dbContextFactory);
            services.AddScoped<IDbContext>(a =>
            {
                return a.GetService<IDbContextFactory>().CreateContext();
            });

            return services;
        }
    }
}
﻿using Chloe.Descriptors;
using Chloe.Exceptions;
using Chloe.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Chloe
{
    public static class ChloeDbContextExtension
    {
        /// <summary>
        /// dbContext.Update&lt;TEntity&gt;(a => a.PrimaryKey == key, a => new TEntity() { IsEnabled = false });
        /// </summary>
        /// <typeparam name="TEntity">必须包含 IsEnabled 属性</typeparam>
        /// <param name="dbContext"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int Disable<TEntity>(this IDbContext dbContext, object key)
        {
            return UpdateIsEnabled<TEntity>(dbContext, key, false);
        }
        /// <summary>
        /// dbContext.Update&lt;TEntity&gt;(a => a.PrimaryKey == key, a => new TEntity() { IsEnabled = true });
        /// </summary>
        /// <typeparam name="TEntity">必须包含 IsEnabled 属性</typeparam>
        /// <param name="dbContext"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int Enable<TEntity>(this IDbContext dbContext, object key)
        {
            return UpdateIsEnabled<TEntity>(dbContext, key, true);
        }
        /// <summary>
        /// dbContext.Update&lt;TEntity&gt;(a => a.PrimaryKey == key, a => new TEntity() { IsDeleted = true, DeletionTime = deletionTime });
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbContext"></param>
        /// <param name="key"></param>
        /// <param name="deletionTime">传 null 则会将 DateTime.Now 值更新至数据库</param>
        /// <returns></returns>
        public static int SoftDelete<TEntity>(this IDbContext dbContext, object key, DateTime? deletionTime = null)
        {
            Type entityType = typeof(TEntity);

            List<MemberBinding> bindings = new List<MemberBinding>();
            bindings.Add(GetMemberAssignment(entityType, "IsDeleted", true, false));
            bindings.Add(GetMemberAssignment(entityType, "DeleteTime", deletionTime ?? DateTime.Now));

            return dbContext.Update<TEntity>(key, bindings);
        }
        /// <summary>
        /// dbContext.Update&lt;TEntity&gt;(a => a.PrimaryKey == key, a => new TEntity() { IsDeleted = true, DeletionTime = DateTime.Now, DeleteUserId = deleteUserId });
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbContext"></param>
        /// <param name="key"></param>
        /// <param name="deleteUserId"></param>
        /// <returns></returns>
        public static int SoftDelete<TEntity>(this IDbContext dbContext, object key, object deleteUserId)
        {
            return dbContext.SoftDelete<TEntity>(key, DateTime.Now, deleteUserId);
        }
        /// <summary>
        /// dbContext.Update&lt;TEntity&gt;(a => a.PrimaryKey == key, a => new TEntity() { IsDeleted = true, DeletionTime = deletionTime, DeleteUserId = deleteUserId });
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbContext"></param>
        /// <param name="key"></param>
        /// <param name="deletionTime"></param>
        /// <param name="deleteUserId"></param>
        /// <returns></returns>
        public static int SoftDelete<TEntity>(this IDbContext dbContext, object key, DateTime deletionTime, object deleteUserId)
        {
            Type entityType = typeof(TEntity);

            List<MemberBinding> bindings = new List<MemberBinding>();
            bindings.Add(GetMemberAssignment(entityType, "IsDeleted", true, false));
            bindings.Add(GetMemberAssignment(entityType, "DeleteTime", deletionTime));
            bindings.Add(GetMemberAssignment(entityType, "DeleteUserId", deleteUserId));

            return dbContext.Update<TEntity>(key, bindings);
        }

        static void SetValueIfNeeded(object entity, TypeDescriptor typeDescriptor, string propertyName, object valueToSet)
        {
            PropertyInfo pi = typeDescriptor.Definition.Type.GetProperty(propertyName);
            if (pi != null && pi.PropertyType.GetUnderlyingType() == valueToSet.GetType())
            {
                var mappingMemberDescriptor = typeDescriptor.TryGetPropertyDescriptor(pi);
                if (mappingMemberDescriptor != null)
                {
                    var value = mappingMemberDescriptor.GetValue(entity);
                    if (value.IsDefaultValueOfType(mappingMemberDescriptor.PropertyType))
                    {
                        mappingMemberDescriptor.SetValue(entity, valueToSet);
                    }
                }
            }
        }

        static Expression<Func<TEntity, bool>> BuildPredicate<TEntity>(object key)
        {
            /*
             * key:
             * 如果实体是单一主键，则传入的 key 与主键属性类型相同的值
             * 如果实体是多主键，则传入的 key 须是包含了与实体主键类型相同的属性的对象，如：new { Key1 = "1", Key2 = "2" }
             */
            Type entityType = typeof(TEntity);
            TypeDescriptor typeDescriptor = EntityTypeContainer.GetDescriptor(entityType);
            EnsureEntityHasPrimaryKey(typeDescriptor);

            KeyValuePairList<MemberInfo, object> keyValueMap = new KeyValuePairList<MemberInfo, object>();

            if (typeDescriptor.PrimaryKeys.Count == 1)
            {
                keyValueMap.Add(typeDescriptor.PrimaryKeys[0].Property, key);
            }
            else
            {
                /*
                 * key: new { Key1 = "1", Key2 = "2" }
                 */

                object multipleKeyObject = key;
                Type multipleKeyObjectType = multipleKeyObject.GetType();

                for (int i = 0; i < typeDescriptor.PrimaryKeys.Count; i++)
                {
                    var keyMemberDescriptor = typeDescriptor.PrimaryKeys[i];
                    MemberInfo keyMember = multipleKeyObjectType.GetProperty(keyMemberDescriptor.Property.Name);
                    if (keyMember == null)
                        throw new ArgumentException(string.Format("The input object does not define property for key '{0}'.", keyMemberDescriptor.Property.Name));

                    object value = keyMember.GetMemberValue(multipleKeyObject);
                    if (value == null)
                        throw new ArgumentException(string.Format("The primary key '{0}' could not be null.", keyMemberDescriptor.Property.Name));

                    keyValueMap.Add(keyMemberDescriptor.Property, value);
                }
            }

            ParameterExpression parameter = Expression.Parameter(entityType, "a");
            Expression lambdaBody = null;

            foreach (var keyValue in keyValueMap)
            {
                Expression propOrField = Expression.PropertyOrField(parameter, keyValue.Key.Name);
                Expression wrappedValue = Chloe.Extensions.ExpressionExtension.MakeWrapperAccess(keyValue.Value, keyValue.Key.GetMemberType());
                Expression e = Expression.Equal(propOrField, wrappedValue);
                lambdaBody = lambdaBody == null ? e : Expression.AndAlso(lambdaBody, e);
            }

            Expression<Func<TEntity, bool>> predicate = Expression.Lambda<Func<TEntity, bool>>(lambdaBody, parameter);

            return predicate;
        }
        static int UpdateIsEnabled<TEntity>(IDbContext dbContext, object key, bool isEnabled)
        {
            Type entityType = typeof(TEntity);

            List<MemberBinding> bindings = new List<MemberBinding>();
            bindings.Add(GetMemberAssignment(entityType, "IsEnabled", isEnabled, false));

            return dbContext.Update<TEntity>(key, bindings);
        }
        static int Update<TEntity>(this IDbContext dbContext, object key, List<MemberBinding> bindings)
        {
            Type entityType = typeof(TEntity);
            NewExpression newExp = Expression.New(entityType);

            ParameterExpression parameter = Expression.Parameter(entityType, "a");
            Expression lambdaBody = Expression.MemberInit(newExp, bindings);

            Expression<Func<TEntity, TEntity>> lambda = Expression.Lambda<Func<TEntity, TEntity>>(lambdaBody, parameter);
            Expression<Func<TEntity, bool>> condition = BuildPredicate<TEntity>(key);

            return dbContext.Update(condition, lambda);
        }

        static void ThrowIfPropIsNull(Type entityType, PropertyInfo prop, string propName)
        {
            if (prop == null)
                throw new ArgumentException(string.Format("The type '{0}' doesn't define property '{1}'", entityType.FullName, propName));
        }
        static MemberAssignment GetMemberAssignment(Type entityType, string propName, object bindValue, bool makeWrapperAccess = true)
        {
            PropertyInfo prop = entityType.GetProperty(propName);
            ThrowIfPropIsNull(entityType, prop, propName);

            Expression exp = makeWrapperAccess ? Chloe.Extensions.ExpressionExtension.MakeWrapperAccess(bindValue, prop.PropertyType) : Expression.Constant(bindValue, prop.PropertyType);

            MemberAssignment bind = Expression.Bind(prop, exp);

            return bind;
        }

        static void EnsureEntityHasPrimaryKey(TypeDescriptor typeDescriptor)
        {
            if (!typeDescriptor.HasPrimaryKey())
                throw new ChloeException(string.Format("The entity type '{0}' does not define any primary key.", typeDescriptor.Definition.Type.FullName));
        }
    }
}

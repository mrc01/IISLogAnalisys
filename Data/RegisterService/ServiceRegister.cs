﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Mrc.Data
{ 
   public static class ServiceRegister
    {

        public static void RegisterAppServices(this IServiceCollection services)
        {
            List<Type> implementationTypes = ServiceFinder.FindFromCompileLibraries();
            RegisterAppServices(services, implementationTypes);
        }

        public static void RegisterAppServices(this IServiceCollection services, Assembly assembly)
        {
            List<Type> implementationTypes = ServiceFinder.Find(assembly);
            RegisterAppServices(services, implementationTypes);
        }

        public static void RegisterAppServicesFromDirectory(this IServiceCollection services, string path)
        {
            List<Type> implementationTypes = ServiceFinder.FindFromDirectory(path);
            RegisterAppServices(services, implementationTypes);
        }

        static void RegisterAppServices(IServiceCollection services, List<Type> implementationTypes)
        {
            Type appServiceType = typeof(IMrcService);
            foreach (Type implementationType in implementationTypes)
            {
                var implementedAppServiceTypes = implementationType.GetTypeInfo().ImplementedInterfaces.Where(a => a != appServiceType && appServiceType.IsAssignableFrom(a));

                foreach (Type implementedAppServiceType in implementedAppServiceTypes)
                {
                    if (typeof(IDisposable).IsAssignableFrom(implementationType))
                        services.AddScoped(implementedAppServiceType, implementationType);
                    else
                        services.AddTransient(implementedAppServiceType, implementationType);
                }
            }
        }


    }
}

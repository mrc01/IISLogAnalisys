﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Data
{
    public  class GlobalsConfig
    {
        public static IServiceProvider Services { get; set; }
        public static IConfiguration Configuration { get; set; }

        public static string ContentRootPath { get; set; }

        public static string WebRootPath { get; set; }

        public static string cacheType { get; set; }
        public static string EncryptKey { get; set; }

        public static void SetBaseConfig(IConfiguration config, string contentRootPath, string webRootPath)
        {
            Configuration = config;
            ContentRootPath = contentRootPath;
            WebRootPath = webRootPath;
            EncryptKey = config["Key:EncryptKey"];
            cacheType = config["Cache:type"];
        }
    }
}

# IISLogAnalisys

#### 介绍

基于asp.net core2.2开发的项目 目前已实现功能权限（权限-角色-组织-岗位-用户）

登录可选择使用redis或者普通的cookies

其他功能持续更新中。。。。

#### 软件架构

开发框架：asp.net core 2.2  基于chloe.orm官方ace项目框架改写 

数据库：sqlserver

ORM: Chloe.ORM

缓存：cookies+Redis+rabbitMQ

开发环境：VS2017
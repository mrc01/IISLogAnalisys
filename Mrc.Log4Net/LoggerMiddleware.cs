﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
namespace Mrc.Log4Net
{
    public static class LoggingServiceCollectionExtensions
    {
        public static IServiceCollection AddLog4Net(this IServiceCollection services)
        {
            services.AddLogging(builder =>
            {
                builder.AddProvider(new Log4NetProvider($"{Directory.GetCurrentDirectory()}\\log4net.config"));
            });
            return services;
        }
    }
}

USE [IISLogAnalisys]
GO

/****** Object:  Table [dbo].[SysC2CIM]    Script Date: 2019/6/20 21:32:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SysC2CIM](
	[Id] [nvarchar](50) NOT NULL,
	[FromUid] [nvarchar](250) NULL,
	[ToUid] [nvarchar](250) NULL,
	[FromName] [nvarchar](250) NULL,
	[ToName] [nvarchar](250) NULL,
	[Msg] [nvarchar](max) NULL,
	[MsgType] [int] NULL,
	[Time] [bigint] NULL,
	[IsRead] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[DeleteTime] [datetime] NULL,
	[DeleteUserId] [varchar](500) NULL,
 CONSTRAINT [PK_C2CIM] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


